//
//  BasicInfoTableViewCell.swift
//  Sayarat
//
//  Created by macbook on 3/11/21.
//

import UIKit

class BasicInfoTableViewCell: UITableViewCell {
    static var identifier = "BasicInfoTableViewCell"

    @IBOutlet var labelInfo: UILabel!
    @IBOutlet var value: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
