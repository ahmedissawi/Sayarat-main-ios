//
//  CarFeatureTableViewCell.swift
//  Sayarat
//
//  Created by macbook on 4/8/21.
//

import UIKit

class CarFeatureTableViewCell: UITableViewCell {
    static var identifier = "CarFeatureTableViewCell"

    @IBOutlet var feature: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
