//
//  HomesTableViewCell.swift
//  Sayarat
//
//  Created by macbook on 5/12/21.
//

import UIKit
import SDWebImage

class HomesTableViewCell: UITableViewCell {

    static var identifier = "HomesTableViewCell"
    var carUser:Cars?
    var homeCar:Datas?

    @IBOutlet var Name: UILabel!
    @IBOutlet var location: UILabel!
    
    @IBOutlet var price: UILabel!
    @IBOutlet var speed: UILabel!
    @IBOutlet var pump: UILabel!
    @IBOutlet var car: UILabel!
    @IBOutlet var Automation: UILabel!
    @IBOutlet var userOfCar: UILabel!
    
    
    @IBOutlet var carImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureHome(homeCar:Datas){
        self.homeCar = homeCar
        Name.text = homeCar.name
        
        Name.font = UIFont(name: "Ubuntu", size: 14)
        let pri=homeCar.price ?? 0
        if pri == 0 {
            price.text = "contact seller"

        }else{
            price.text = "\(pri)"

        }
       
        price.font = UIFont(name: "Ubuntu", size: 14)

        speed.text = homeCar.mileage ?? ""  + "Km"
        speed.font = UIFont(name: "Ubuntu", size: 14)

        userOfCar.text = homeCar.owner?.name
        userOfCar.font = UIFont(name: "Ubuntu", size: 14)

        Automation.text = homeCar.engine?.name
        Automation.font = UIFont(name: "Ubuntu", size: 14)

        pump.text = homeCar.fuel?.name
        pump.font = UIFont(name: "Ubuntu", size: 14)

        car.text = homeCar.car_model
        car.font = UIFont(name: "Ubuntu", size: 14)
        carImg.sd_setImage(with:URL(string:homeCar.photos?[0] ?? ""))
        location.text = homeCar.owner?.address

    }
    
    
    func configure(carUser:Cars){

        self.carUser = carUser
        Name.text = carUser.name
        
        Name.font = UIFont(name: "Ubuntu", size: 14)
        
        
        
        let pri=carUser.price ?? 0
        if pri == 0 {
            price.text = "contact seller"

        }else{
            price.text = "\(pri)"

        }
        
        
        price.text = "\(carUser.price ?? 0)"
        price.font = UIFont(name: "Ubuntu", size: 14)

        speed.text = carUser.mileage ?? ""  + "Km"
        speed.font = UIFont(name: "Ubuntu", size: 14)

        userOfCar.text = carUser.owner?.name
        userOfCar.font = UIFont(name: "Ubuntu", size: 14)

        Automation.text = carUser.engine?.name
        Automation.font = UIFont(name: "Ubuntu", size: 14)

        pump.text = carUser.fuel?.name
        pump.font = UIFont(name: "Ubuntu", size: 14)

        car.text = carUser.car_model
        car.font = UIFont(name: "Ubuntu", size: 14)
        print(carUser.photos)
        carImg.sd_setImage(with:URL(string:carUser.photos?[0] ?? ""))
        
        location.text = carUser.owner?.address


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }
    
}
