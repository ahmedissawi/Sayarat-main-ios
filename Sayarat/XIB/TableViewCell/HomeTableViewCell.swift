//
//  HomeTableViewCell.swift
//  Sayarat
//
//  Created by macbook on 3/10/21.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    static var identifier = "HomeTableViewCell"

    @IBOutlet var Name: UILabel!
    @IBOutlet var location: UILabel!
    
    @IBOutlet var price: UILabel!
    @IBOutlet var speed: UILabel!
    @IBOutlet var pump: UILabel!
    @IBOutlet var car: UILabel!
    @IBOutlet var Automation: UILabel!
    @IBOutlet var userOfCar: UILabel!
    
    
    @IBOutlet var carImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
