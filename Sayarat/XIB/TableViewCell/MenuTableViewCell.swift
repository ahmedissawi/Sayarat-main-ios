//
//  MenuTableViewCell.swift
//  Sayarat
//
//  Created by macbook on 3/10/21.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    static var identifier = "MenuTableViewCell"

    @IBOutlet var imgName: UIImageView!
    @IBOutlet var labelName: UILabel!
    var item :Maincell?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configure(){
        
        if let object = item{
            labelName.text = object.labelName
            imgName.image = object.imgName
    }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
