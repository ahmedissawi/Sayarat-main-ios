//
//  CarImageCollectionViewCell.swift
//  Sayarat
//
//  Created by macbook on 4/2/21.
//

import UIKit

class CarImageCollectionViewCell: UICollectionViewCell {
    static var identifier = "CarImageCollectionViewCell"

    @IBOutlet var photo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
