//
//  SimilarCarCollectionViewCell.swift
//  Sayarat
//
//  Created by macbook on 3/12/21.
//

import UIKit

class SimilarCarCollectionViewCell: UICollectionViewCell {
    static var identifier = "SimilarCarCollectionViewCell"

    @IBOutlet var carImg: UIImageView!
    @IBOutlet var Name: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var speed: UILabel!
    @IBOutlet var pump: UILabel!
    @IBOutlet var door: UILabel!
    @IBOutlet var auto: UILabel!
    @IBOutlet var user: UILabel!
    


    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
