//
//  Clousre.swift
//  Sayarat
//
//  Created by macbook on 8/16/21.
//

import Foundation

typealias ObjectRequestCallback<T> = ( _ status: Bool, _ object: T?) -> Void
typealias ProcessRequestCallback = ( _ status: Bool) -> Void
typealias ListRequestCallback<T> = ( _ status: Bool, _ list: [T?]) -> Void
typealias InternetConnectionChecker = ( _ status: Bool) -> Void
