//
//  AppDelegate.swift
//  Sayarat
//
//  Created by macbook on 3/9/21.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleMobileAds
let googleApiKey = "AIzaSyC602LpIBnzRd4u3qgNEfW-QVu98f9HO_Y"

extension UIStoryboard{
    
    static let mainStoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
}
@main
class AppDelegate: UIResponder, UIApplicationDelegate{
    var rootViewController = UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "LGSideViewControllers")
//    private func updateLanguage(newLang: String){
//        let mainStoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
//
//             let rootViewController: UIWindow = ((UIApplication.shared.delegate?.window)!)!
//        rootViewController.rootViewController = mainStoryBoard.instantiateViewController(withIdentifier: "LGSideViewControllers")
//        if !L102Language.currentAppleLanguage().elementsEqual(newLang){
//            L102Language.changeLanguage(view: rootViewController, newLang: newLang, rootViewController: "rootViewController")
//        }
//    }

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        
        L102Localizer.DoTheMagic()
       // L102Language.setAppleLAnguageTo(lang: "en")
//        updateLanguage(newLang: L102Language.currentAppleLanguage() == "en" ? "ar" : "en")
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        IQKeyboardManager.shared.enable = true
        GMSServices.provideAPIKey(googleApiKey)
        if #available(iOS 13, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.backgroundColor = UIColor(red: 191.0/255.0, green: 37.0/255.0, blue: 40.0/255.0, alpha: 1.0)
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance

        }
//        FirebaseApp.configure()
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
      
        return true
    }
    

    

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return ApplicationDelegate.shared.application(
            app,
            open: url,
            options: options
        )
    }
    func loginButton(_ loginButton: FBLoginButton!, didCompleteWith result: LoginManagerLoginResult!, error: Error!) {
      if let error = error {
        print(error.localizedDescription)
        return
      }
      // ...
    }

}

