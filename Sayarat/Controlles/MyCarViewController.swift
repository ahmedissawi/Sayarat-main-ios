//
//  MyCarViewController.swift
//  Sayarat
//
//  Created by macbook on 4/22/21.
//

import UIKit
import GoogleMobileAds
class MyCarViewController: UIViewController {
    @IBOutlet var bannerAds: GADBannerView!
    var car:[Cars]=[]

        var fullName:String = ""
    var count:Int=0
    @IBOutlet var displayTable: UITableView!
    var fuelType:[String]=[]
        var BodyType:[String]=[]
        var gearbox:[String]=[]
        var model:[String]=[]
        var make:[String]=[]
        var userName:[String]=[]
        var millege:[String]=[]
        var price:[String]=[]
        var documentid:[String]=[]
        var address:[String]=[]
        var username:[String]=[]

        var img:[String]=[]
        var userUrl:URL?

    
override func viewDidLoad() {
        super.viewDidLoad()
    bannerAds.adUnitID = "ca-app-pub-7967951980951384/3323556351"
    bannerAds.load(GADRequest())
    bannerAds.rootViewController=self

    navigationController?.navigationBar.isHidden = false
    navigationItem.title = "My Cars".localized
        registerNib()
    getDataProfile()
    self.showLoading()
        displayTable.delegate=self
        displayTable.dataSource=self
    displayTable.reloadData()
    self.hideLoading()
    }
    
    
    func getDataProfile(){
       internetConnectionChecker { (status) in
           if status{
               self.showLoading()
       MenuApiController.myCar { (status, user) in
           if status{
               
               self.hideLoading()
               self.car = user ?? []
               self.displayTable.reloadData()
               
       }
           self.hideLoading()

       }
           }else{
               self.showAlertMessage(title: "Error".localized, message: "No internet connection".localized)
           }
       }
    }
    
func registerNib(){
        let nib = UINib(nibName: "HomesTableViewCell", bundle: nil)
        displayTable.register(nib, forCellReuseIdentifier: "HomesTableViewCell")
    }
    
}

extension MyCarViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return car.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomesTableViewCell", for: indexPath) as! HomesTableViewCell

        cell.selectionStyle = .none

        cell.configure(carUser: car[indexPath.row])
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "PreviewUserInfoViewController")  as! PreviewUserInfoViewController
        UserDefaults.standard.set(car[indexPath.row].id, forKey: "documentid")
        vc.carPhoto = car[indexPath.row].photos ?? [""]
        vc.carPrice = car[indexPath.row].price
        vc.isFromMyCars = true

//        vc.userinfo=user[indexPath.row]

        navigationController?.pushViewController(vc, animated: true)
    }
    }



