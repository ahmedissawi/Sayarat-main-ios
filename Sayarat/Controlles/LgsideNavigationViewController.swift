//
//  LgsideNavigationViewController.swift
//  Sayarat
//
//  Created by macbook on 5/1/21.
//

import UIKit

class LgsideNavigationViewController: UINavigationController,UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("LGSide")
        
        let menuItem = UIBarButtonItem(image: UIImage(named: "align"), style:.plain, target: self, action: #selector(showMenu))
        self.navigationBar.topItem?.leftBarButtonItem = menuItem
        delegate=self
        // Do any additional setup after loading the view.
    }
    
    @objc func showMenu(){
        
        
                if lang() == "ar"{
                    self.showRightView(self)
        
                }else{
                    self.showLeftView(self)
        
                }
        
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController is HomeViewController || viewController is MyCarViewController || viewController is AddCarInfoViewController || viewController is EditProfileViewController || viewController is AboutUsViewController || viewController is ContactUsViewController || viewController is PrivacyPolicyViewController{
            addMenuButton(view: viewController)

        }
        
    }
    
    public func addMenuButton(view:UIViewController){
        let menuItem = UIBarButtonItem(image: UIImage(named: "align"), style:.plain, target: self, action: #selector(showMenu))
        view.navigationItem.leftBarButtonItem=menuItem

        
        
    }
    public func hideMenu(){
        if lang() == "ar"{
            self.hideRightView(self)

        }else{
            self.hideLeftView(self)

        }

        
        
    }
}
