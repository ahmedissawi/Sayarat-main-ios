//
//  MapKitMarkerViewController.swift
//  Sayarat
//  Created by macbook on 4/21/21.


import UIKit
import MapKit
import CoreLocation
import GoogleMaps
import SDWebImage

class MapKitMarkerViewController: UIViewController {

    var lat:String = ""
    var long:String  = ""
    var carDName:String=""
    var carDprice:String=""
    var carDsaloon:String=""
    var carDautomatic:String=""
    var carDmillege:String=""
    var carDfuel:String=""
    var carDimg:String=""

    @IBOutlet var mapView: GMSMapView!
    
    @IBOutlet var carCart: UIViewDesignable!
    @IBOutlet var userImg: UIImageViewDesignable!
    @IBOutlet var usera: UILabel!
    @IBOutlet var automatic: UILabel!
    @IBOutlet var door: UILabel!
    @IBOutlet var disel: UILabel!
    @IBOutlet var speed: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var carName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLocation(pdblLatitude: lat, pdblLongitude: long)
        navigationController?.navigationBar.isHidden = false
        navigationItem.title =  NSLocalizedString("Location".localized, comment: "")
        
    
        
        automatic.text=UserDefaults.standard.string(forKey: "carengine")
        door.text=UserDefaults.standard.string(forKey: "carsaloon")
        disel.text=UserDefaults.standard.string(forKey: "carfuel")
        speed.text=UserDefaults.standard.string(forKey: "carmillege")
        price.text=UserDefaults.standard.string(forKey: "cardatap")
        carName.text=UserDefaults.standard.string(forKey: "catdatan")
        userImg.sd_setImage(with: URL(string: carDimg), completed: nil)

        
        
        
        
//        self.setLocation(pdblLatitude:  "31.418472402930576", pdblLongitude: "34.34977613389492")

    }

}

extension MapKitMarkerViewController{
    

    

    func setLocation(pdblLatitude: String,pdblLongitude: String){
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lats: Double = Double(pdblLatitude) ?? 0.0
        print(lats)

        //21.228124
        let lons: Double = Double(pdblLongitude) ?? 0.0
        print(lons)

        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lats
        center.longitude = lons
        print( center.latitude)
        print( center.longitude)

        let camera = GMSCameraPosition.camera(withLatitude:  center.latitude, longitude: center.longitude, zoom: 6.0)
//        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.camera=camera
        mapView.frame=CGRect.zero
        //        view = mapView
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude:  center.latitude, longitude: center.longitude)
//        marker.title = "CarLocation"
//        marker.snippet = "Australia"
        marker.map = mapView

    }

}

