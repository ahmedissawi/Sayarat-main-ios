//
//  PreviewUserInfoViewController.swift
//  Sayarat
//
//  Created by macbook on 3/12/21.

import UIKit
import FSPagerView
import SDWebImage
class PreviewUserInfoViewController: UIViewController {
    private let sectionInsets = UIEdgeInsets(
      top: 0,
      left: 20,
      bottom: 0,
      right: 20)
    private let itemsPerRow: CGFloat = 2.574074074074074
    var itemSelected:Int?
    var fuelT:String=""
    var bodyT:String=""
    var millege:String=""
    var gearb:String=""
    var infos:[String]=[]
    var year:String=""
    var color:String=""
    var serviceHistory:String=""
    var img = ""
    var userUrl:URL?
    var isFromMyCars = false
    var photoCount=0
    var carDetails:Car?
    var similarCar:[Similar_cars]=[]
    var carPhoto:[String]=[]
    var carPrice:Int?
    @IBOutlet var containerHeight: NSLayoutConstraint!
    @IBOutlet var basicInfoCollection: UICollectionView!
    var info:[Maincellss] = []
    var noItems = 3

    @IBOutlet var carName: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var pagerViews: FSPagerView!{
        didSet {
               self.pagerViews.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
               self.pagerViews.itemSize = FSPagerView.automaticSize
           }
        
    }
    
    @IBOutlet var pagerControls: FSPageControl!{
        didSet {
            self.pagerControls.numberOfPages = photoCount
            self.pagerControls.contentHorizontalAlignment = .center
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title =  "Car Details".localized

            registerNib1()
            initialize()
    
        
        
        if isFromMyCars{
            let totalHeight = 340
            containerHeight.constant = CGFloat(totalHeight)
            photoCount = carPhoto.count
            price.text = "\(carPrice ?? 0)"

        }else{
            let totalHeight = 630
            containerHeight.constant = CGFloat(totalHeight)
            if let _car = carDetails,let _photo=_car.photos{
                 photoCount = _photo.count
                carPhoto=_photo
                carName.text = _car.name
                
                
                let pri=_car.price ?? 0
                if pri == 0 {
                    price.text = "contact seller"

                }else{
                    price.text = "\(pri)"

                }
                
                
                

                
            }
        }

     
    //        let refreshButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.refresh, target: self, action: #selector(buttonMethod))
    //            navigationItem.leftBarButtonItem = refreshButton


        carName.font = UIFont(name: "Ubuntu", size: 18)

        carName.font = UIFont(name: "Ubuntu", size: 18)

            basicInfoCollection.delegate = self
            basicInfoCollection.dataSource = self
            pagerViews.delegate = self
            pagerViews.dataSource = self

            pagerViews.isInfinite = true
            pagerViews.automaticSlidingInterval = 3.0
            self.pagerViews.transformer = FSPagerViewTransformer(type: .linear)
            let transform = CGAffineTransform(scaleX: 0.8, y: 0.9)
             self.pagerViews.itemSize = self.pagerViews.frame.size.applying(transform)
        
            self.pagerViews.decelerationDistance = FSPagerView.automaticDistance;
    //        self.pageView.interitemSpacing = 5
    //        self.pageView.decelerationDistance = 2
    //        self.pageView.itemSize = CGSize.init(width: self.pageView.frame.width, height: self.pageView.frame.height * 0.8)

        
        
        pagerControls.numberOfPages=photoCount
            pagerControls.contentHorizontalAlignment = .center

            pagerControls.setImage(UIImage(named:"notSelected"), for: .normal)
            pagerControls.setImage(UIImage(named:"selected"), for: .selected)
        millege=UserDefaults.standard.string(forKey: "carmillege") ?? ""
        fuelT=UserDefaults.standard.string(forKey: "carfuel") ?? ""
        bodyT=UserDefaults.standard.string(forKey: "carsaloon") ?? ""
        gearb=UserDefaults.standard.string(forKey: "carengine") ?? ""

            infos.append(millege)
            infos.append(fuelT)
            infos.append(bodyT)
            infos.append(gearb)


            
        
    }
    
    func registerNib1(){
        let nib = UINib(nibName: "FeaturesCollectionViewCell", bundle: nil)
        basicInfoCollection.register(nib, forCellWithReuseIdentifier: "FeaturesCollectionViewCell")
    }

    func initialize(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
            self.itemSelected = 0
            self.basicInfoCollection.reloadData()
        }
        info.append(Maincellss.speed)
        info.append(Maincellss.gaspump)
        info.append(Maincellss.door)
        info.append(Maincellss.auto)
        
        
        
    }
}
extension PreviewUserInfoViewController: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
   
        return info.count
            
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeaturesCollectionViewCell", for: indexPath) as! FeaturesCollectionViewCell
        
        let objj=self.info[indexPath.row]
        cell.item = objj
        cell.imgF.image = info[indexPath.row].imgName
        cell.feature.text=infos[indexPath.row]

            return cell
       
        
        
    }
    

}
extension PreviewUserInfoViewController:FSPagerViewDelegate,FSPagerViewDataSource{

    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return photoCount
    }
    
    
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.sd_setImage(with: URL(string:carPhoto[index]))
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pagerControls.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pagerControls.currentPage = pagerView.currentIndex
    }
    

}
