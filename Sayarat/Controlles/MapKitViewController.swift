//
//  MapKitViewController.swift
//  Sayarat
//
//  Created by macbook on 3/11/21.
//

import UIKit
import MapKit
import CoreLocation
import GoogleMaps

struct Stadium {
  var name: String
  var lattitude: CLLocationDegrees
  var longtitude: CLLocationDegrees
}
class MapKitViewController: UIViewController{
    var lat:CLLocationDegrees?
    var lag:CLLocationDegrees?
    var marker: GMSMarker!
    var countryCode:String=""
    var country:String=""
    var address:String=""
    private let locationManager = CLLocationManager()
    @IBOutlet weak var addressLabel: UILabel!
    var latitude = 0.0
        var longitude = 0.0
        var centerMapCoordinate:CLLocationCoordinate2D!
    @IBOutlet var mapLocation: GMSMapView!

//    let stadiums = [Stadium(name: "Emirates Stadium", lattitude: 51.5549, longtitude: -0.108436),
//                    Stadium(name: "Stamford Bridge", lattitude: 51.4816, longtitude: -0.191034),
//                    Stadium(name: "White Hart Lane", lattitude: 51.6033, longtitude: -0.065684),
//                    Stadium(name: "Olympic Stadium", lattitude: 51.5383, longtitude: -0.016587),
//                    Stadium(name: "Old Trafford", lattitude: 53.4631, longtitude: -2.29139),
//                    Stadium(name: "Anfield", lattitude: 53.4308, longtitude: -2.96096)]
//
    
    override func viewDidLoad() {
      super.viewDidLoad()
      checkLocationServices()
        locationManager.delegate = self

        locationManager.requestWhenInUseAuthorization()

        mapLocation.delegate = self
           locationManager.desiredAccuracy = kCLLocationAccuracyBest
           locationManager.startUpdatingLocation()

        guard let locValue = CLLocationManager.init().location?.coordinate  else { return }
        print("locations = \(locValue.latitude) \(locValue.latitude)")
        
        getAddressFromLatLon(pdblLatitude:"\(locValue.latitude)", withLongitude: "\(locValue.longitude)")
        let location = CLLocation(latitude: locValue.latitude, longitude:locValue.longitude)
        location.fetchCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            print(city + ", " + country)  // Rio de Janeiro, Brazil
        }
    }
    func checkLocationServices() {
      if CLLocationManager.locationServicesEnabled() {
//        checkLocationAuthorization()
      } else {
        // Show alert letting the user know they have to turn this on.
      }
    }

    func fetchStadiumsOnMap(_ stadiums: [Stadium]) {
      for stadium in stadiums {
        let annotations = MKPointAnnotation()
        annotations.title = stadium.name
        annotations.coordinate = CLLocationCoordinate2D(latitude: stadium.lattitude, longitude: stadium.longtitude)
//        mapLocation.addAnnotation(annotations)
        print( annotations.title)

        
      }
    }

    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
            var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
            //21.228124
            let lon: Double = Double("\(pdblLongitude)")!
            //72.833770
            let ceo: CLGeocoder = CLGeocoder()
            center.latitude = lat
            center.longitude = lon

            let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


            ceo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    let pm = placemarks! as [CLPlacemark]

                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country)
                        print(pm.locality)
                        print(pm.subLocality)
                        print(pm.thoroughfare)
                        print(pm.postalCode)
                        print(pm.subThoroughfare)
                        var addressString : String = ""
                        
                         if pm.isoCountryCode != nil {
                             addressString = addressString + pm.isoCountryCode! + ""
                             self.countryCode = pm.isoCountryCode!
                            print("Country Code \(self.countryCode)")
                         }
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ""
                        }
                        if pm.thoroughfare != nil {
                            addressString =  addressString + pm.thoroughfare! + ""
                        }
                        if pm.locality != nil {
                            addressString =  addressString + pm.locality! + ""
                        }
                        if pm.country != nil {
                            addressString =  addressString + pm.country! + ""
                            self.country = pm.country!
                            print("Country \(pm.country!)")
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }


                        
                       self.address = addressString
                       print("addressString", addressString)
                        UserDefaults.standard.setValue(self.address, forKey: "address")
                        UserDefaults.standard.setValue(pdblLatitude, forKey: "lat")
                        UserDefaults.standard.setValue(pdblLongitude, forKey: "lag")


                    }
            })

        }


}
// MARK: - CLLocationManagerDelegate
//1
extension MapKitViewController: CLLocationManagerDelegate {
  // 2
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    // 3
    guard status == .authorizedWhenInUse else {
      return
    }
    // 4
    locationManager.startUpdatingLocation()
      
    //5
    mapLocation.isMyLocationEnabled = true
    mapLocation.settings.myLocationButton = true
  }
  
  // 6
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.first else {
      return
    }
      
    // 7
    mapLocation.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
      
    // 8
    locationManager.stopUpdatingLocation()
  }
}


extension CLLocation {
    func fetchCityAndCountry(from location: CLLocation,completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
            print(placemarks?.first?.locality)
            print(placemarks?.first?.country)

            
        }



    }


}
extension MapKitViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        //        latitude = mapView.camera.target.latitude
        //        longitude = mapView.camera.target.longitude
        //
        //        centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        //        self.placeMarkerOnCenter(centerMapCoordinate:centerMapCoordinate)
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        latitude = coordinate.latitude
         longitude = coordinate.longitude
         
        centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.placeMarkerOnCenter(centerMapCoordinate:centerMapCoordinate)
        getAddressFromLatLon(pdblLatitude: "\(latitude)", withLongitude: "\(longitude)")

        print("latitude \(latitude) ... longitude \(longitude)")
        
       
    }

    
    func placeMarkerOnCenter(centerMapCoordinate:CLLocationCoordinate2D) {
        
        if marker == nil {
            marker = GMSMarker()
        }
        marker.position = centerMapCoordinate
        marker.map = self.mapLocation
        
        
        
    }
    }
