//
//  BasicInfoViewController.swift
//  Sayarat
//
//  Created by macbook on 3/11/21.
//

import UIKit
import GoogleMobileAds
class BasicInfoViewController: UIViewController, GADFullScreenContentDelegate {
    @IBOutlet var bannerAds: GADBannerView!
    var interstitial: GADInterstitialAd?

    var object :[Infocell] = []
    var info:[String]=[]
    var carDetails:[String]=[]
    @IBOutlet var displayTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationItem.leftBarButtonItem?.action=#selector(putAd)
        bannerAds.adUnitID = "ca-app-pub-7967951980951384/8675971290"
        bannerAds.load(GADRequest())
        bannerAds.rootViewController=self
        carDetails=[]
        info = UserDefaults.standard.stringArray(forKey: "carbasic") ?? []

        object.append(Infocell.Mileage)
        object.append(Infocell.BodyType)
        object.append(Infocell.year)
        object.append(Infocell.FuelType)
        object.append(Infocell.Gearbox)
        object.append(Infocell.Color)
        object.append(Infocell.Service_History)
        loadInterstitial()
        navigationController?.navigationBar.isHidden = false
        navigationItem.title =  "Basic info".localized
                registerNib()
        
        
        
        print(object.count)
        print(info.count)

        displayTable.delegate=self
        displayTable.dataSource=self
        
        
     
    
    }
    @objc func putAd(){
        do {
            try interstitial?.canPresent(fromRootViewController: self)

        }catch{
            
        }
            
       
    }
    
    func registerNib(){
        let nib = UINib(nibName: "BasicInfoTableViewCell", bundle: nil)
        displayTable.register(nib, forCellReuseIdentifier: "BasicInfoTableViewCell")
    }
    
    fileprivate func loadInterstitial() {
      let request = GADRequest()
        
      GADInterstitialAd.load(
        withAdUnitID: "ca-app-pub-7967951980951384/8675971290", request: request
      ) { (ad, error) in
        if let error = error {
          print("Failed to load interstitial ad with error: \(error.localizedDescription)")
          return
        }
        self.interstitial = ad
        self.interstitial?.fullScreenContentDelegate = self
        
        
        do {
            try self.interstitial?.canPresent(fromRootViewController: self)

        }catch{
            
        }
        
      }
    
            
       
    }
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
      print("Ad did present full screen content.")
    }

    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error)
    {
      print("Ad failed to present full screen content with error \(error.localizedDescription).")
    }

    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        loadInterstitial()
    }
    
}
extension BasicInfoViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return info.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BasicInfoTableViewCell", for: indexPath) as! BasicInfoTableViewCell
        print(object.count)
        cell.selectionStyle = .none

        cell.labelInfo.text = object[indexPath.row].labelinfo
        cell.value.text = info[indexPath.row]

        return cell
    }
}

