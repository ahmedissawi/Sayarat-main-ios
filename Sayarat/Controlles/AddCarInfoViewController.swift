//
//  AddCarInfoViewController.swift
//  Sayarat
//
//  Created by macbook on 3/13/21.
//

import UIKit
import Foundation
import AVFoundation
import DropDown
import Photos
import MobileCoreServices
import CountryPickerView
import YPImagePicker
class AddCarInfoViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    var lat=0.0
    var longt=0.0
    private let itemsPerRow: CGFloat = 2
    private let sectionInsets = UIEdgeInsets(
        top: 8,
        left: 20,
        bottom: 0,
        right: 20)
    var SellerCar:CarData?
    
    var country:[String]=[]
    var countryId:[Int]=[]
    
    var engine:[String]=[]
    var engineId:[Int]=[]
    
    var saloon:[String]=[]
    var saloonId:[Int]=[]
    
    var carType:[String]=[]
    var carTypeId:[Int]=[]
    
    var fuel:[String]=[]
    var fuelId:[Int]=[]
    var countryids=0
    var fuelIds=0
    var carTypeIds=0
    var saloonIds=0
    var engineIds=0
    var servicehistorysIds=0

    var servicehistorys:[String]=[]
    var servicehistorysId:[Int]=[]
    
    let dropDown = DropDown()
    var addFeature:[String]=[]
    var imgurl:[String]=[]
    var imgName:[String]=[]
    
    var allNumber:String = ""
    var cpv = CountryPickerView()
    private var imagesArray:[UIImage]=[]
    
    @IBOutlet var codeNum: UILabel!
    @IBOutlet var img: UIImageView!
    @IBOutlet var phoneNum: BottomBorderTextField!
    @IBOutlet var displayTable: UICollectionView!
    @IBOutlet var features: BottomBorderTextField!
    @IBOutlet var carDtailsStack: UIStackView!
    @IBOutlet var locationStack: UIView!
    @IBOutlet var featuresStack: UIView!
    @IBOutlet var basicInfoStack: UIView!
    @IBOutlet var descriptionStack: UIView!
    
    
    @IBOutlet var postedin: UITextField!
    @IBOutlet var bodyType: UIButton!
    @IBOutlet var gearbox: UIButton!
    
    @IBOutlet var fuelTypeLabel: UITextField!
    
    @IBOutlet var gearboxLabel: UITextField!
    @IBOutlet var bodytypeLabel: UITextField!
    @IBOutlet var fuelType: UIButton!
    
    @IBOutlet var carName: UITextField!
    @IBOutlet var payment: UISegmentedControl!
    
    @IBOutlet var used: UISegmentedControl!
    @IBOutlet var price: UITextField!
    
    @IBOutlet var make: UITextField!
    
    @IBOutlet var makee: UIButton!
    
    @IBOutlet var model: UITextField!
    @IBOutlet var millege: UITextField!
    
    @IBOutlet var year: UITextField!
    @IBOutlet var serviceHistory: UITextField!
    @IBOutlet var descriptionText: UITextView!
    @IBOutlet var loc: UITextField!
    
    @IBOutlet var color: UITextField!
    @IBOutlet var serviceAction: UIButton!
    
    @IBOutlet var locAction: UIButton!
    
    
    
    
    func setCountryPickerView() {
        cpv = CountryPickerView(frame:CGRect(x: 0, y: 0, width: 120, height: 20))
        cpv.delegate = self
        cpv.dataSource = self
        img.image = cpv.selectedCountry.flag
        codeNum.text = cpv.selectedCountry.phoneCode
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCountries()
        navigationController?.navigationBar.isHidden = false
        navigationItem.title = "Sell Your Car".localized
        registerNib()
        setCountryPickerView()
        
        displayTable.delegate=self
        displayTable.dataSource=self
        UserDefaults.standard.setValue("", forKey: "address")
        self.payment.setTitle("Cash".localized, forSegmentAt: 0)
        self.payment.setTitle("Cheque".localized, forSegmentAt: 1)
        self.used.setTitle("New".localized, forSegmentAt: 0)
        self.used.setTitle("Used".localized, forSegmentAt: 1)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        loc.text=""
//        loc.text=UserDefaults.standard.string(forKey: "address") ?? "This place is not registed"
        lat = UserDefaults.standard.double(forKey:  "lat")
        longt = UserDefaults.standard.double(forKey: "lag")
        
    }
    
    @IBAction func locActionBtn(_ sender: Any) {
        
        self.dropDown.anchorView = self.locAction
        self.dropDown.dataSource = self.country
        self.dropDown.cellNib = UINib(nibName: "DropDownView", bundle: nil)
        
        self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        }
        self.dropDown.selectionAction = { [self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            countryids = countryId[index]
            self.loc.textColor = UIColor.black
            self.loc.text = item
            
        }
        self.dropDown.width = self.make.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.show()
        
    }
    @IBAction func setLocation(_ sender: Any) {
        let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "MapKitViewController")
        
        navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func codeNumbers(_ sender: Any) {
        cpv.showCountriesList(from: self)
        
    }
    func registerNib(){
        let nib = UINib(nibName: "CarImageCollectionViewCell", bundle: nil)
        displayTable.register(nib, forCellWithReuseIdentifier: "CarImageCollectionViewCell")
    }
    
    @IBAction func service(_ sender: Any) {
        
        self.dropDown.anchorView = self.serviceAction
        self.dropDown.dataSource = self.servicehistorys
        self.dropDown.cellNib = UINib(nibName: "DropDownView", bundle: nil)
        
        self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        }
        self.dropDown.selectionAction = { [self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.serviceHistory.textColor = UIColor.black
            self.serviceHistory.text = item
            servicehistorysIds = servicehistorysId[index]

        }
        self.dropDown.width = self.make.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.show()
    }
    @IBAction func makeAction(_ sender: Any) {
        self.dropDown.anchorView = self.makee
        self.dropDown.dataSource = self.carType
        self.dropDown.cellNib = UINib(nibName: "DropDownView", bundle: nil)
        
        self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        }
        self.dropDown.selectionAction = { [self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.make.textColor = UIColor.black
            self.make.text = item
            carTypeIds = carTypeId[index]

            
        }
        self.dropDown.width = self.make.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.show()
    }
    @IBAction func bodyTY(_ sender: Any) {
        self.dropDown.anchorView = self.bodyType
        self.dropDown.dataSource = self.saloon
        self.dropDown.cellNib = UINib(nibName: "DropDownView", bundle: nil)
        
        self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        }
        self.dropDown.selectionAction = { [self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.bodytypeLabel.textColor = UIColor.black
            self.bodytypeLabel.text = item
            saloonIds = saloonId[index]

            
        }
        self.dropDown.width = self.bodytypeLabel.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.show()
    }
    
    @IBAction func fuelTY(_ sender: Any) {
        self.dropDown.anchorView = self.fuelType
        self.dropDown.dataSource = self.fuel
        self.dropDown.cellNib = UINib(nibName: "DropDownView", bundle: nil)
        
        self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        }
        self.dropDown.selectionAction = { [self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.fuelTypeLabel.textColor = UIColor.black
            self.fuelTypeLabel.text = item
            fuelIds = fuelId[index]

        }
        self.dropDown.width = self.fuelTypeLabel.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        
        self.dropDown.show()
    }
    
    @IBAction func gearboxx(_ sender: Any) {
        self.dropDown.anchorView = self.gearbox
        self.dropDown.dataSource = self.engine
        self.dropDown.cellNib = UINib(nibName: "DropDownView", bundle: nil)
        
        self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        }
        self.dropDown.selectionAction = { [self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.gearboxLabel.textColor = UIColor.black
            self.gearboxLabel.text = item
            engineIds = engineId[index]

            
        }
        self.dropDown.width = self.gearboxLabel.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        
        self.dropDown.show()
    }
    
    
    @IBAction func Save(_ sender: Any) {
        internetConnectionChecker { (status) in
            if status{
                if self.checkData(){
                    self.sellCar()
                }else{
                    
                }
                
            }
        }
    }
    
    @IBAction func AddMore(_ sender: Any) {
        
        if (features.text?.isEmpty == false){
            addFeature.append(features.text!)
            features.text=""
            showAlertMessage(title: "Sucess", message: "the feature added")
            
        }
    }
    
    @IBAction func upload(_ sender: Any) {
        self.imagesArray=[]
        imgurl=[]
        
        var config = YPImagePickerConfiguration()
        config.library.maxNumberOfItems = 20
        let picker = YPImagePicker(configuration: config)
        picker.didFinishPicking { [self, unowned picker] items, cancelled in
            for item in items {
                switch item {
                case .photo(let photo):
                    
                    self.imgName.append(photo.asset?.localIdentifier.description ?? "")
                    imagesArray.append(photo.originalImage)
                    displayTable.reloadData()
                    print("PHOTO", photo.originalImage,  imagesArray.count)
                    
                case .video(let video):
                    print(video)
                }
                
            }
            displayTable.reloadData()
            
            //              self.setUsersPhotoURL(Image: self.imagesArray, FileName:self.imgName)
            
            picker.dismiss(animated: true, completion: nil)
        }
        present(picker, animated: true, completion: nil)
        
    }
}
extension AddCarInfoViewController{
    func saveDatadone(){
        carName.text!=""
        price.text!=""
        millege.text!=""
        fuelTypeLabel.text!=""
        gearboxLabel.text!=""
        bodytypeLabel.text!=""
        make.text!=""
        model.text!=""
        year.text!=""
        color.text!=""
        serviceHistory.text!=""
        descriptionText.text=""
        addFeature=[]
        allNumber=""
        imagesArray=[]
    }
    func checkData()-> Bool{
        allNumber+="\(phoneNum.text!)"
        
        if !carName.text!.isEmpty &&  !price.text!.isEmpty && !payment.isSelected && !used.isSelected && !millege.text!.isEmpty && !fuelTypeLabel.text!.isEmpty &&  !gearboxLabel.text!.isEmpty &&  !bodytypeLabel.text!.isEmpty &&  !make.text!.isEmpty &&  !model.text!.isEmpty &&  !year.text!.isEmpty &&  !color.text!.isEmpty &&  !serviceHistory.text!.isEmpty &&  !descriptionText.text.isEmpty && !addFeature.isEmpty && !allNumber.isEmpty {
            return true
            
        }else{
            showAlertMessage(title: "Error", message: "Please enter  All required data")
            return false
            
        }
        
    }
    
    //    func setUsersPhotoURL(Image: [UIImage], FileName: [String]) {
    //
    //        imgurl=[]
    //       if Image.count != 0{
    //        for index in 0 ... Image.count-1{
    //
    //            guard let imageData = Image[index].jpegData(compressionQuality: 0.5) else { return }
    //        let storageRef = Storage.storage().reference()
    //        let thisUserPhotoStorageRef = storageRef.child("carImages").child(FileName[index])
    //
    //        let uploadTask = thisUserPhotoStorageRef.putData(imageData, metadata: nil) { (metadata, error) in
    //            guard let metadata = metadata else {
    //                print("error while uploading")
    //                return
    //            }
    //            thisUserPhotoStorageRef.downloadURL { (url, error) in
    //                print(metadata.size) // Metadata contains file metadata such as size, content-type.
    //                thisUserPhotoStorageRef.downloadURL { (url, error) in
    //                    guard let downloadURL = url else {
    //                        print("an error occured after uploading and then getting the URL")
    //                        return
    //                    }
    //
    //                    let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
    //                    changeRequest?.photoURL = downloadURL
    //                    guard let imgesUrl = changeRequest?.photoURL else{return }
    //
    //                    self.imgurl.append("\(imgesUrl)")
    //                    print(self.imgurl)
    //                    print(self.imgurl.count)
    //
    //                    changeRequest?.commitChanges { (error) in
    //
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    }
    //}
}
extension AddCarInfoViewController:UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarImageCollectionViewCell", for: indexPath) as! CarImageCollectionViewCell
        
        cell.photo.image = imagesArray[indexPath.row]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: 120)
        
    }
    
    
}


extension AddCarInfoViewController: CountryPickerViewDelegate, CountryPickerViewDataSource{
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        
        codeNum.text = country.phoneCode
        img.image = country.flag
        allNumber = "\(codeNum.text!)"
        
    }
    
    
}

extension AddCarInfoViewController{
    
    func getCountries(){
        country=[]
        countryId=[]
        engine=[]
        engineId=[]
        saloon=[]
        saloonId=[]
        carType=[]
        carTypeId=[]
        fuel=[]
        fuelId=[]
        servicehistorys=[]
        servicehistorysId=[]
        internetConnectionChecker { (statuss) in
            if statuss{
        CarApiController.Setting { (status, country,engine,saloon,carType,fuels,servicehistory) in
            if status{
                
                guard let _country=country else {return}
                guard let _engine=engine else {return}
                guard let _saloon=saloon else {return}
                guard let _carType=carType else {return}
                guard let _fuels=fuels else {return}
                guard let _servicehistory=servicehistory else {return}
                
                for index in 0 ... _country.count-1{
                    self.country.append(_country[index].name ?? "")
                    self.countryId.append(_country[index].id ?? 0)
                    
                }
                
                for index in 0 ... _engine.count-1{
                    
                    self.engine.append(_engine[index].name ?? "")
                    self.engineId.append(_engine[index].id ?? 0)
                    
                }
                
                
                for index in 0 ... _saloon.count-1{
                    self.saloon.append(_saloon[index].name ?? "")
                    self.saloonId.append(_saloon[index].id ?? 0)
                    
                    
                }
                for index in 0 ... _carType.count-1{
                    self.carType.append(_carType[index].name ?? "")
                    self.carTypeId.append(_carType[index].id ?? 0)
                    
                    
                }
                
                for index in 0 ... _fuels.count-1{
                    self.fuel.append(_fuels[index].name ?? "")
                    self.fuelId.append(_fuels[index].id ?? 0)
                    
                    
                }
                for index in 0 ... _servicehistory.count-1{
                    self.servicehistorys.append(_servicehistory[index].name ?? "")
                    self.servicehistorysId.append(_servicehistory[index].id ?? 0)
                    
                    
                }
                
                
            }
            
        }
            }
        }
    }
 
}

extension AddCarInfoViewController{
    
    func makeParmaeter() -> [String:Any]{
    
        
        var newArry : [[String:Any]] = []
        for index in 0...addFeature.count-1{
           
            newArry.append(["features[\(index)]": "\(addFeature[index])"])
            
        }
        
        for indexx in 0...imagesArray.count-1{
           
            newArry.append(["images[\(indexx)]" : "\(imagesArray[indexx])"])
            
        }
        
        let result = newArry.compactMap { $0 }.reduce([:]) { $0.merging($1) { (current, _) in current } }
        print(result)
        return result
    }
}
extension AddCarInfoViewController{
    
    func sellCar(){
        internetConnectionChecker { (status) in
            if status{
                self.SellerCar = CarData(price: Int(self.price.text!) ?? 0, payment_method: self.payment.titleForSegment(at: self.payment.selectedSegmentIndex)!, car_status: self.used.titleForSegment(at: self.used.selectedSegmentIndex)!, mileage: Int(self.millege.text!) ?? 0, saloon_id: self.saloonIds, fuel_id: self.fuelIds, engine_id: self.engineIds, car_type_id: self.carTypeIds, car_model: self.model.text!, year: Int(self.year.text!) ?? 0, color: self.color.text!, service_history_id:  self.servicehistorysIds , country_id: self.countryids, latitude: self.lat, longitude: self.longt, mobile: Int(self.allNumber) ?? 0, name_en: self.carName.text!, details_en: self.descriptionText.text!)
                guard let car = self.SellerCar else {return}
                self.showLoading()
                CarApiController.AddCar( images:self.imagesArray, car: car, param: self.makeParmaeter()) { (status, message) in
                    self.hideLoading()
                    self.showAlertMessage(title: "Sayarat", message: message)

                }

            }else{
                self.showAlertMessage(title: "Error", message: "No internetConnection")
            }
        }


    }
    
    
    
}
