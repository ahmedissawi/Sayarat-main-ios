//
//  FilterViewController.swift
//  Sayarat
//
//  Created by macbook on 4/25/21.
//

import UIKit
import DropDown
class FilterViewController: UIViewController {
    let dropDown = DropDown()
    var country:[String]=[]
    var countryId:[Int]=[]
    var carType:[String]=[]
    var carTypeId:[Int]=[]
    var user:FilterData?
    var idCar=0
    var idCountry=0
    var payment = "cash"
    var onFilterDissmissed : OnFilterDissmissed?
    
    @IBOutlet var makeLabel: UITextField!
    @IBOutlet var price1: BottomBorderTextField!
    
    @IBOutlet var price2: BottomBorderTextField!
    @IBOutlet var paymentMethod: UISegmentedControl!
    @IBOutlet var countAction: UIButton!
    
    @IBOutlet var countryTxtField: UITextField!
    @IBOutlet var makee: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCountries()
    }
    
    @IBAction func b0(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        switch paymentMethod.selectedSegmentIndex {
        case 0:
            self.payment = "cash"
        case 1:
            self.payment = "cheque"
        default:
            break;
        }
    }

    
    @IBAction func countriresBtn(_ sender: Any) {
        self.dropDown.anchorView = self.countAction
        self.dropDown.dataSource = self.country
        self.dropDown.cellNib = UINib(nibName: "DropDownView", bundle: nil)
        
        self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        }
        self.dropDown.selectionAction = { [self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.countryTxtField.textColor = UIColor.black
            self.countryTxtField.text = item
            idCountry=countryId[index]
            
        }
        self.dropDown.width = self.countryTxtField.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.show()
        
        
    }
    @IBAction func Filter(_ sender: Any) {
        filterDataUser()
    }
    @IBAction func makeAction(_ sender: Any) {
        self.dropDown.anchorView = self.makee
        self.dropDown.dataSource = self.carType
        self.dropDown.cellNib = UINib(nibName: "DropDownView", bundle: nil)
        
        self.dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
        }
        self.dropDown.selectionAction = { [self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.makeLabel.textColor = UIColor.black
            self.makeLabel.text = item
            idCar=carTypeId[index]
        }
        self.dropDown.width = self.makeLabel.frame.size.width
        self.dropDown.bottomOffset = CGPoint(x: 0, y:(self.dropDown.anchorView?.plainView.bounds.height)!)
        self.dropDown.show()
    }
}

extension FilterViewController{
    
    func getCountries(){
        country=[]
        countryId=[]
        carType=[]
        carTypeId=[]
        
        CarApiController.Setting { (status, country,engine,saloon,carType,fuels,servicehistory) in
            if status{
                guard let _country=country else {return}
                guard let _carType=carType else {return}
                
                for index in 0 ... _country.count-1{
                    self.country.append(_country[index].name ?? "")
                    self.countryId.append(_country[index].id ?? 0)
                    
                }
                for index in 0 ... _carType.count-1{
                    self.carType.append(_carType[index].name ?? "")
                    self.carTypeId.append(_carType[index].id ?? 0)
                    
                    
                }
                
                
                
            }
            
            
            
        }
    }
    func filterDataUser(){
        internetConnectionChecker { (status) in
            if status{
                self.showLoading()
                self.user=FilterData(car_type_id: self.idCar, payment_method: self.payment, country_id: self.idCountry, price_from: Int(self.price1.text!)!, price_to: Int(self.price2.text!)!)

                guard let _user = self.user else {return}
                self.hideLoading()
                UserApiController.FilterData(data: _user) { (status, users) in
                    self.hideLoading()

                    if status{
                        
                     
                        self.dismiss(animated: true) {
                            if let _delegate = self.onFilterDissmissed{
                                _delegate.filteredData(data:users ?? [] )
                            }}

                        
                    }else{
                        self.showAlertMessage(title: "Error", message: "There's no data")
                    }
                }
                
            }else{
                self.showAlertMessage(title: "Error", message: "No internet connection")
            }
        }
   
        
        
        
        
    }
    
}
