//
//  AboutUsViewController.swift
//  Sayarat
//
//  Created by macbook on 3/14/21.
//

import UIKit

class AboutUsViewController: UIViewController {

    @IBOutlet var laAboutUs: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
        navigationItem.title =  NSLocalizedString("About Us".localized, comment: "")
        aboutUs()
    }
    

}

extension AboutUsViewController{
    func aboutUs(){
        internetConnectionChecker { (status) in
            if status{
                self.showLoading()
                MenuApiController.AboutUs { (status, message) in
                    self.hideLoading()
                    self.laAboutUs.text = message
                }
            }
        }
    }
    
/////
    
    
}
