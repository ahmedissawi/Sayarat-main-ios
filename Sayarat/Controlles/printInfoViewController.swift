

import UIKit
import MessageUI
import SDWebImage
class printInfoViewController: UITableViewController {
//    UserDefaults.standard.set(self.Automation, forKey: "Automation")
//    UserDefaults.standard.set(self.saloon, forKey: "saloon")

    var caruser:Car?
    var userUrl:URL?
    let composeViewController = MFMailComposeViewController()
    var gearbox :[String]=[]
    var price:[String]=[]
    var fuelType :[String]=[]
    var carName :[String]=[]
    var carprice :[String]=[]
    var automatic :[String]=[]
    var saloon :[String]=[]
    var millege:[String]=[]
    var carimg:[String]=[]
    
    
    var countN:Int=0
    var uid=""
    @IBOutlet var icon1: UIImageView!
    @IBOutlet var icon2: UIImageView!
    @IBOutlet var icon3: UIImageView!
    @IBOutlet var icon4: UIImageView!
    var fullName:String = ""
    var address:String = ""
    var img = ""
    var carN = ""
    var whats = ""
    var email = ""
    private let sectionInsets = UIEdgeInsets(
      top: 8,
      left: 20,
      bottom: 0,
      right: 20)
    private let itemsPerRow:CGFloat = 2

    @IBOutlet var staticTableViewInfo: UITableView!
    
    @IBOutlet var similarCarCollection: UICollectionView!
    var documentid = ""
    var carPrice = ""

    
    var carDName:String=""
    var carDprice:String=""
    var carDsaloon:String=""
    var carDautomatic:String=""
    var carDmillege:String=""
    var carDfuel:String=""
    var carDimg:String=""

    
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var place: UILabel!
    var info:[Maincellss] = []
    var car:[Similar_cars] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        userName.text=UserDefaults.standard.string(forKey: "ownerName")
        place.text=UserDefaults.standard.string(forKey: "ownerAddress")
        whats=UserDefaults.standard.string(forKey: "ownerMobile") ?? ""
        email=UserDefaults.standard.string(forKey: "ownerEmail") ?? ""
        userImage.sd_setImage(with: URL(string: UserDefaults.standard.string(forKey: "ownerImg") ?? ""), completed: nil)
        registerNib1()
        carDName=UserDefaults.standard.string(forKey: "catdatan") ?? ""
        carDprice=UserDefaults.standard.string(forKey: "cardatap") ?? ""
        carDsaloon=UserDefaults.standard.string(forKey: "carsaloon") ?? ""
        carDautomatic=UserDefaults.standard.string(forKey: "carengine") ?? ""
        carDmillege=UserDefaults.standard.string(forKey: "carmillege") ?? ""
        carDfuel=UserDefaults.standard.string(forKey: "carfuel") ?? ""
        carDimg = UserDefaults.standard.string(forKey: "cardatapic") ?? ""
        
 

       fuelType = UserDefaults.standard.stringArray(forKey: "fuel") ?? []
        carName = UserDefaults.standard.stringArray(forKey: "carSname") ?? []
        carprice = UserDefaults.standard.stringArray(forKey: "carSprice") ?? []
         automatic = UserDefaults.standard.stringArray(forKey: "Automation") ?? []
        saloon = UserDefaults.standard.stringArray(forKey: "saloon") ?? []
       millege = UserDefaults.standard.stringArray(forKey: "millege") ?? []
        carimg = UserDefaults.standard.stringArray(forKey: "carphoto") ?? []
        
     
                controlBackItem(backItem: icon1)
        controlBackItem(backItem: icon2)
        controlBackItem(backItem: icon3)
        controlBackItem(backItem: icon4)

//        car=UserDefaults.standard.array(forKey: "similarCar") as? [Similar_cars] ?? []
//        print(documentid)

        carN = UserDefaults.standard.string(forKey: "CarName") ?? ""
        print(carN)

        carPrice = UserDefaults.standard.string(forKey:"price") ?? ""
        print(carPrice)

        similarCarCollection.dataSource = self
        similarCarCollection.dataSource = self
   
//        self.getid(id:UserDefaults.standard.string( forKey: "documentid") ?? "")
        
        print(carN)

//        self.getuserNames(id: self.uid)
    }
    
    @IBAction func callAction(_ sender: Any) {
//        guard whats != "" else{
//            self.showAlertMessage(title: "Error".localized, message: "No phone number".localized)
//            return
//        }
        callNumber(phoneNumber: whats)
    }
    
    @IBAction func message(_ sender: Any) {
        if email != ""{
            callEmail(email:email)
        }

    }
    
    @IBAction func whatsApp(_ sender: Any) {
        
        let urlWhats = "whatsapp://send?phone=\(whats)"
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
            if let whatsappURL = URL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL){
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(whatsappURL)
                    }
                }
                else {
                    self.showAlertMessage(title: "Error".localized, message: "The phone is invalid".localized)

                }
            }
        }

    }
    
    func registerNib1(){
        let nib = UINib(nibName: "SimilarCarCollectionViewCell", bundle: nil)
        similarCarCollection.register(nib, forCellWithReuseIdentifier: "SimilarCarCollectionViewCell")
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch (indexPath.section, indexPath.row) {
        case (0,0):
            let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "BasicInfoViewController")
            
            vc.title = "BasicInfo"
        
            navigationController?.pushViewController(vc, animated: true)
            
        case (0,1):
            
            let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "DescriptionViewController")
            
            vc.title = "Description"
            navigationController?.pushViewController(vc, animated: true)
            
        case (0,2):
            let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "FeaturesViewController")
            
            vc.title = "Features"
            navigationController?.pushViewController(vc, animated: true)
            
            
        case (0,3):
            let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "MapKitMarkerViewController") as! MapKitMarkerViewController
            vc.long = UserDefaults.standard.string(forKey: "longitude") ?? ""
            vc.lat = UserDefaults.standard.string(forKey: "latitude") ?? ""
            vc.carDName=carDName
            vc.carDprice=carDprice
            vc.carDsaloon=carDsaloon
            vc.carDautomatic=carDautomatic
            vc.carDmillege=carDmillege
            vc.carDfuel=carDfuel

            navigationController?.pushViewController(vc, animated: false)
            
        default:
            print("")
        }
    }
    
    
    
}
extension printInfoViewController: UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(price)

        return carName.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SimilarCarCollectionViewCell", for: indexPath) as! SimilarCarCollectionViewCell
        print(price)
        cell.Name.text=carName[indexPath.row]
        cell.price.text=carprice[indexPath.row]
        cell.pump.text=fuelType[indexPath.row]
        cell.speed.text = millege[indexPath.row]
        cell.auto.text=automatic[indexPath.row]
        cell.door.text=saloon[indexPath.row]
        cell.carImg.sd_setImage(with: URL(string: carimg[indexPath.row]), completed: nil)
 

        return cell
        
        
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 350 , height: 350)
//    }


}
extension printInfoViewController{
    func controlBackItem(backItem: UIImageView){
       if L102Language.currentAppleLanguage().elementsEqual("ar"){
           if let _img = backItem.image {
               backItem.image =  UIImage(cgImage: _img.cgImage!, scale:_img.scale , orientation: UIImage.Orientation.upMirrored)
           }
       }
   }
    func callWhatsapp(phoneNum:String){
        if phoneNum == nil || phoneNum == "" {
            showAlertMessage(title: "Error", message: "you should add your phone number")

            
        }else{
            
        
    let phoneNumber =  phoneNum
let appURL = URL(string: "https://wa.me/\(phoneNumber)")!
    if UIApplication.shared.canOpenURL(appURL) {
        
        UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
    }
    }
    }

    func callEmail(email:String){
        if email == nil{
            showAlertMessage(title: "Error", message: "you should add your email")

        }else{
            
        

        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self as? MFMailComposeViewControllerDelegate
            mail.setToRecipients([email])
            mail.setMessageBody("<h1>Hello there<h1>", isHTML: true)
            present(mail, animated: true)
        } else {
            self.showAlertMessage(title: "Error".localized, message: "Cannot send email".localized)
            print("Cannot send email")
        }
        func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
            controller.dismiss(animated: true)
        }
    }
    }
    
     func callNumber(phoneNumber:String) {
        
        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                    
                }
            }
        }else{
            self.showAlertMessage(title: "Error".localized, message: "The phone is invalid".localized)
        }
    }
    
    func callMobile(mobileNum:String){
        if mobileNum == nil || mobileNum == "" {
            showAlertMessage(title: "Error", message: "you should add your phone number")

            
        }else{
            
        let number = Int(mobileNum)
            let url:NSURL = URL(string: "TEL://\(number!)") as? NSURL ?? NSURL()
            print(url)

        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    
    }

}

// MARK: Localizable
public protocol Localizable {
    var localized: String { get }
}

extension String: Localizable {
    public var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
