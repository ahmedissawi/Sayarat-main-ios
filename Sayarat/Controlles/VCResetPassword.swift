//
//  VCResetPassword.swift
//  Sayarat
//
//  Created by macbook on 5/29/21.
//

import UIKit
import AuthenticationServices
class VCResetPassword: UIViewController {
var test = false
    @IBOutlet var emailTxtField: BottomBorderTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false


        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendAction(_ sender: Any) {
        if  !emailTxtField.text!.isEmpty {
            resetPassword()
        }
    }


}

extension VCResetPassword{
    
        func resetPassword(){
            internetConnectionChecker { (status) in
                if status{
                    self.showLoading()
                    UserApiController.resetPassword(email: self.emailTxtField.text!) { (status, msg) in
                        self.hideLoading()
                        self.showAlertMessage(title: "Sayarat", message: msg)
                        
                    }
                }
            }
         
            
    }
  
    
}

