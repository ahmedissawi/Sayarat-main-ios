//
//  MenuViewController.swift
//  Sayarat
//
//  Created by macbook on 3/10/21.
//

import UIKit
import SDWebImage
class MenuViewController: UIViewController {
    var objects:[Maincell]=[]
    var car:[Cars]=[]
    var fullName:String = ""
//    var address:String = ""
    var img = ""
    var userUrl:URL?
    var imgUrl:URL?
    var name=""
    var num = 0
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var Address: UILabel!
    @IBOutlet var displayTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        MyCar()
        registerNib()
        initializer()
        displayTable.delegate=self
        displayTable.dataSource=self
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        
    }
    
    func registerNib(){
        let nib = UINib(nibName: "MenuTableViewCell", bundle: nil)
        displayTable.register(nib, forCellReuseIdentifier: "MenuTableViewCell")
    }
    
    func initializer(){
        objects.append(Maincell.Home)
        objects.append(Maincell.My_Car)
        objects.append(Maincell.Sell_Your_Car)
        objects.append(Maincell.Edit_your_profile)
        objects.append(Maincell.About_Us)
        objects.append(Maincell.Contact_us)
        objects.append(Maincell.Privacy_Policy)
        objects.append(Maincell.Arabic)
        objects.append(Maincell.Logout)
        
        
    }
}
extension MenuViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        
        cell.configure()
        cell.labelName.text = objects[indexPath.row].labelName
        cell.imgName.image = objects[indexPath.row].imgName
        cell.selectionStyle = .none

        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let vc = UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "LGSideViewControllers")
            let rootVc = sideMenuController?.rootViewController as! LgsideNavigationViewController
            
            rootVc.setViewControllers([vc], animated: true)
            rootVc.hideMenu()
            
        case 1:
            let vc = UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "MyCarViewController") as! MyCarViewController
            let rootVc = sideMenuController?.rootViewController as! LgsideNavigationViewController
           // vc.car=car
            rootVc.setViewControllers([vc], animated: true)
            rootVc.hideMenu()
            
        case 2:
            let vc = UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "AddCarInfoViewController")
            let rootVc = sideMenuController?.rootViewController as! LgsideNavigationViewController
            
            rootVc.setViewControllers([vc], animated: true)
            rootVc.hideMenu()
        case 3:
            let vc = UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "EditProfileViewController")
            let rootVc = sideMenuController?.rootViewController as! LgsideNavigationViewController
            
            rootVc.setViewControllers([vc], animated: true)
            rootVc.hideMenu()
        case 4:
            let vc = UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "AboutUsViewController")
            let rootVc = sideMenuController?.rootViewController as! LgsideNavigationViewController
            
            rootVc.setViewControllers([vc], animated: true)
            rootVc.hideMenu()
            
        case 5:
            
            print("case5")
            let vc = UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "ContactUsViewController")
            let rootVc = sideMenuController?.rootViewController as! LgsideNavigationViewController
            
            rootVc.setViewControllers([vc], animated: true)
            rootVc.hideMenu()
        case 6:
            
            let vc = UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController")
            let rootVc = sideMenuController?.rootViewController as! LgsideNavigationViewController
            
            rootVc.setViewControllers([vc], animated: true)
            rootVc.hideMenu()
        case 7:
            print("fdhj")
                    showLanguageActionSheet()
        case 8:
            print("none")
            internetConnectionChecker { (status) in
                if status{
                    MenuApiController.logout { (status, message) in
                        if status{
                            let vc = UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "navigate")
                            vc.modalPresentationStyle = .fullScreen
                            
                            self.present(vc, animated: true, completion: nil)
                        }
                      
                    }
                }
            }
            
        
        default:
            print("none")
        }
    }
    private func showLanguageActionSheet(){
        let languageActionSheet: UIAlertController = UIAlertController(title: NSLocalizedString("choose_language_title", comment: ""), message: NSLocalizedString("choose_language_message", comment: ""), preferredStyle: UIAlertController.Style.actionSheet)

        let enAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("English", comment: ""), style: .default) { (action) in
            self.updateLanguage(newLang: "en")
        }

        let arAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Arabic", comment: ""), style: .default) { (action) in
            self.updateLanguage(newLang: "ar")
        }

        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .destructive, handler: nil)

        languageActionSheet.addAction(enAction)
        languageActionSheet.addAction(arAction)
        languageActionSheet.addAction(cancelAction)

        present(languageActionSheet, animated: true, completion: nil)
    }

    private func updateLanguage(newLang: String){
        if !L102Language.currentAppleLanguage().elementsEqual(newLang){
            L102Language.changeLanguage(view: self, newLang: newLang, rootViewController: "LGSideViewControllers")
        }
    }
}







extension MenuViewController{
    
    
   func getData(){
    MenuApiController.GetProfile { (status, user) in
        if status{
           
        self.userImage.sd_setImage(with: URL(string: user?.image_profile ?? ""))
        self.userName.text = user?.name
        self.Address.text = user?.address
    }
    }
}
}

extension MenuViewController{
    
    func MyCar(){
        internetConnectionChecker { (statuss) in
            if statuss{
                self.showLoading()
                MenuApiController.myCar { (status, carUser) in
                    self.car = carUser ?? []
                    self.hideLoading()
                }
            }else{
                self.showAlertMessage(title: "Error", message: "Please check your internet connection")
            }
        }
     
    }
    
    
    
    
    
    
    
}
