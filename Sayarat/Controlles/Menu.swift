//
//  Menu.swift
//  
//
//  Created by macbook on 10/9/20.
//  Copyright © 2020 macbook. All rights reserved.
//


import Foundation
import UIKit
class Menu{
    public var labelName:String?
    public var labelinfo:String?

    public var imgName:UIImage?

    init(labelName:String,img:UIImage,labelinfo:String) {
        self.labelName=labelName
        self.imgName = img
        self.labelinfo = labelinfo
        
    }
}

enum Maincell{
    case Home
    case My_Car
    case Sell_Your_Car
    case Edit_your_profile

    case About_Us
    case Contact_us
    case Privacy_Policy
    case Arabic
    case Logout

    
    
    var labelName:String{
        
        switch self {
        case .Home:
            return NSLocalizedString("Home", comment: "")

        case .My_Car:
            return NSLocalizedString("My Car", comment: "")

        case .Sell_Your_Car:
            return NSLocalizedString("Sell Your Car", comment: "")

        case .Edit_your_profile:
            return NSLocalizedString("Edit profile", comment: "")

        case .About_Us:
            return NSLocalizedString("About Us", comment: "")

        case .Contact_us:
            return NSLocalizedString("Contact us", comment: "")

        case .Privacy_Policy:
            return NSLocalizedString("Privacy Policy", comment: "")

        case .Arabic:
            return NSLocalizedString("Language", comment: "")
        case .Logout:
            return NSLocalizedString("Logout", comment: "")

            
        }
    }
    var imgName:UIImage?{
        
        switch self {
        case .Home:
            return UIImage(named: "home")
        case .My_Car:
            return UIImage(named: "car")
        case .Sell_Your_Car:
            return UIImage(named: "plus-circle")
        case .Edit_your_profile:
            return UIImage(named: "edit")
        case .About_Us:
            return UIImage(named: "alert-circle")
        case .Contact_us:
            return UIImage(named: "Contact_US")
        case .Privacy_Policy:
            return UIImage(named: "file")
        case .Arabic:
            return UIImage(named: "globe")
        case .Logout:
            return UIImage(named: "power")
            
        }
    }

}
enum Infocell{
    
    case Mileage
    case BodyType
    case year
    case FuelType
    case Gearbox
    case Color
    case Service_History
  

    var labelinfo:String{
        
        switch self {
        case .Mileage:
            return "Mileage"
        case .BodyType:
            return "Body Type"

        case .FuelType:
            return "Fuel Type"
        case .Gearbox:
            return "Gearbox"
        case .year:
            return "Year"
        case .Color:
            return "Color"
        case .Service_History:
            return "Service History"

        }
    }
    
    
    
 
}

