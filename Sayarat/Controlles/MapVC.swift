//
//  MapVC.swift
//  Sayarat
//
//  Created by Ahmed Issawi on 26/01/2022.
//

import UIKit
import GoogleMaps
protocol MapCoordinatesDelgate : class {
    
    func SelectedDone(Lat: Double, Lon: Double, address:String )
}


class MapVC: UIViewController {
    
    var mapView = GMSMapView()
    var searchFiledView = UIView()
    var searchFiled = UITextField()
    var MapDelegate:MapCoordinatesDelgate?
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    var lon:Double = 0.0
    var lat:Double = 0.0
    var address:String = ""
    var marker = false

    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        mapView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        enableLocationServices()
        checkUsersLocationServicesAuthorization()
        
    }

    func setUI(){
        
        //Create UI Map View Controller
        
        /* Create MapView */
        mapView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mapView)

        mapView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        mapView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        mapView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        //view.safeAreaLayoutGuide.bottomAnchor
        
      
        /* Create Button */
  
        
        let CancelButton = UIButton()
        CancelButton.translatesAutoresizingMaskIntoConstraints = false
        CancelButton.setTitle("Cancel".localized, for: .normal)
        CancelButton.backgroundColor = UIColor(red: 191.0/255.0, green: 37.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        CancelButton.setTitleColor(.white, for: .normal)
        CancelButton.titleLabel?.font = .systemFont(ofSize: 16)
        CancelButton.layer.cornerRadius = 5
        CancelButton.clipsToBounds = true
        CancelButton.addTarget(self, action: #selector(self.CancelbuttonTapped), for: .touchUpInside)
        view.addSubview(CancelButton)

        CancelButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        CancelButton.widthAnchor.constraint(equalToConstant: 300).isActive = true
        CancelButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        CancelButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: -40).isActive = true
        
        let DoneButton = UIButton()
        DoneButton.translatesAutoresizingMaskIntoConstraints = false
        DoneButton.setTitle("Pick".localized, for: .normal)
        DoneButton.backgroundColor = UIColor(red: 191.0/255.0, green: 37.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        DoneButton.setTitleColor(.white, for: .normal)
        DoneButton.titleLabel?.font = .systemFont(ofSize: 16)
        DoneButton.layer.cornerRadius = 5
        DoneButton.clipsToBounds = true
        DoneButton.addTarget(self, action: #selector(self.DonebuttonTapped), for: .touchUpInside)
        view.addSubview(DoneButton)

        DoneButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        DoneButton.widthAnchor.constraint(equalToConstant: 300).isActive = true
        DoneButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        DoneButton.bottomAnchor.constraint(equalTo: CancelButton.bottomAnchor,constant: -80).isActive = true
  
        
    }
    
    @objc func DonebuttonTapped(sender : UIButton) {
        if marker{
            self.dismiss(animated: true) {
                self.MapDelegate?.SelectedDone(Lat: self.lat, Lon: self.lon, address: self.address)
            }
        }
    }
    
    @objc func CancelbuttonTapped(sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
  
  
    func getLanguage() -> String {
        guard let appleLanguages = UserDefaults.standard.object(forKey: "AppleLanguages") as? [String] else { return "" }
        let prefferedLanguage = appleLanguages[0]
        if prefferedLanguage.contains("-") {
            let array = prefferedLanguage.components(separatedBy: "-")
            return array[0]
        }
        return prefferedLanguage
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        enableLocationServices()
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        self.marker = true
        self.lat = coordinate.latitude
        self.lon = coordinate.longitude
        self.mapView.getAddressFromLatLon(pdblLatitude: String(self.lat), withLongitude: String(self.lon)) { address in
            self.searchFiled.text = address
            self.address = address
        }

        mapView.clear()
        let position = CLLocationCoordinate2DMake(coordinate.latitude,coordinate.longitude)
        let marker = GMSMarker(position: position)
        
        self.mapView.camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 13)

        let myLocationCamera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 13)
        self.mapView.animate(to: myLocationCamera)
        //    marker.icon = UIImage(named: "checkloaction")
        marker.map = mapView
        
    }
    
    
}

extension MapVC:GMSMapViewDelegate{
    
    func getCurrentLocation() {
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
            currentLocation = locationManager.location
            if currentLocation != nil{
//                self.lat = currentLocation.coordinate.latitude
//                self.lon = currentLocation.coordinate.longitude
                                self.lat = 31.28346170
                                self.lon = 31.28346170
                
            }
            mapView.camera = GMSCameraPosition.camera(withLatitude: self.lat, longitude: self.lon, zoom: 14)
            
            let myLocationCamera = GMSCameraPosition.camera(withLatitude: self.lat, longitude: self.lon, zoom: 14)
            self.mapView.animate(to: myLocationCamera)
            
            mapView.delegate = self
            mapView.isMyLocationEnabled = true
        }
    }
}
extension MapVC{
    
    func checkUsersLocationServicesAuthorization(){
        /// Check if user has authorized Total Plus to use Location Services
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                // Request when-in-use authorization initially
                // This is the first and the ONLY time you will be able to ask the user for permission
                self.locationManager.delegate = self
                locationManager.requestWhenInUseAuthorization()

                break
                
            case .restricted, .denied:
                // Disable location features
                
                let alert = UIAlertController(title: "Allow Location Access".localized, message: "MyApp needs access to your location. Turn on Location Services in your device settings.".localized, preferredStyle: UIAlertController.Style.alert)
                
                // Button to Open Settings
                alert.addAction(UIAlertAction(title: "Settings".localized, style: UIAlertAction.Style.default, handler: { action in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)")
                        })
                    }
                }))
                alert.addAction(UIAlertAction(title: "Ok".localized, style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                break
                
            case .authorizedWhenInUse, .authorizedAlways:
                // Enable features that require location services here.
                print("Full Access")
                break
            }
        }
    }
}


extension MapVC:CLLocationManagerDelegate{
    
    func enableLocationServices() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
            break
            
        case .restricted, .denied:
            // Disable location features
            
            break
            
        case .authorizedWhenInUse:
            // Enable basic location features
            getCurrentLocation()
            locationManager.startUpdatingLocation()
            
            break
            
        case .authorizedAlways:
            // Enable any of your app's location features
            getCurrentLocation()
            locationManager.startUpdatingLocation()
            
            break
            
        default:
            break
        }
    }
    
    
    
}

extension GMSMapView{
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String,handler: @escaping (String) -> Void){
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, preferredLocale: Locale(identifier: L102Language.currentAppleLanguage()), completionHandler:
                                    {(placemarks, error) in
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            if let pm = placemarks{
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country as Any)
                    print(pm.locality as Any)
                    print(pm.subLocality as Any)
                    print(pm.thoroughfare as Any)
                    print(pm.postalCode as Any)
                    print(pm.subThoroughfare as Any)
                    print(pm.isoCountryCode as Any)
                    
                    
                    var addressString : String = ""
                    
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    handler(addressString)
                    
                    print("addressString:\(addressString)")
                    
                }
            }
        })
    }

}
