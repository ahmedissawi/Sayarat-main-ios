//
//  LoginViewController.swift
//  Sayarat
//
//  Created by macbook on 3/14/21.
//

import UIKit
import FBSDKLoginKit
import AuthenticationServices
import CryptoKit

typealias call = (_ status: Bool) -> (Void)

class LoginViewController: UIViewController, LoginButtonDelegate {
    
    let appleProvider = AppleSignInClient()

    var fullName:String = ""
    var email:String = ""
    var socialImg:UIImage?
    var idToken:String=""
    var provider:String=""
    
    var log:SocialLog?
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print ("Error signing out: %@")
        
    }
    
    private var currentnonce:String?
    //    var users:[Contact]=[]
    var iconClick = true
    
    @IBOutlet var passtext: BottomBorderTextField!
    var facebooklogin:Bool = false
    var appleLogin:Bool = false
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if let error = error {
            print("Facebook login with error: \(error.localizedDescription)")
        } else {
            // User logs in successfully and can continue with Firebase Authentication sign-in
        }
    }
    
    
    //    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
    //        let firebaseAuth = Auth.auth()
    //    do {
    //      try firebaseAuth.signOut()
    //    } catch let signOutError as NSError {
    //      print ("Error signing out: %@", signOutError)
    //    }
    //
    //    }
    
    
    
    @IBOutlet var emailTxtfield: BottomBorderTextField!
    @IBOutlet var passwordTxtfield: BottomBorderTextField!
    
    @IBOutlet var f: LoginManager!
    override func viewDidLoad() {
        super.viewDidLoad()
        #if DEBUG
        self.emailTxtfield.text = "ahmed@a.com"
        self.passwordTxtfield.text = "841997"
        #endif
        //        getData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func signAsGuest(_ sender: Any) {
        let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "GuestViewControllers")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func show(_ sender: Any) {
        if(iconClick == true) {
            passtext.isSecureTextEntry = false
        } else {
            passtext.isSecureTextEntry = true
        }
        
        iconClick = !iconClick
        
    }
    @IBAction func login(_ sender: Any) {
        internetConnectionChecker { (status) in
            if status{
                if self.checkData(){
                    self.showLoading()
                    self.performLogin()
                    
                }else{
                    
                }
                
            }
        }
    }
    
    @IBAction func forgetPassword(_ sender: Any) {
        let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "VCResetPassword")
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func signuP(_ sender: Any) {
        let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "SignUpViewController")
        navigationController?.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func signWithFacebook(_ sender: Any) {
                internetConnectionChecker { (status) in
                    if status{
                        self.showLoading()

                        self.loginByFB()
        
                        }
                    }
        
        
        
    }
    
    
        @IBAction func signUpApple(_ sender: Any) {

            appleProvider.handleAppleIdRequest(block: { [self] fullName, email, token in
                self.showLoading()

                loginSocial(email: "\(email ?? "")", name: "\(fullName ?? "")", img: "UIImage()", provider: "apple", idToken: "\(token!)")
         

               print("loginByApple \(fullName) .. \(email) .. \(token)")
                  
            })
            
    
        }
    
}
extension LoginViewController{
    public func checkData()-> Bool {
        
        if !emailTxtfield.text!.isEmpty && !passwordTxtfield.text!.isEmpty {
            return true
        }else{
            showAlertMessage(title: "Error", message: "Please enter  All required data")
            
        }
        return false
    }
    public func performLogin(){
        UserApiController.Login(email: emailTxtfield.text!, password: passwordTxtfield.text!) { (user, status) in
            if status{
                if let _user = user{
                    UserDefaults.standard.setValue("Bearer \(_user.access_token!)", forKey: "Token")
                    self.hideLoading()
                    UserDefaults.standard.set(true, forKey: "isLogin")
                    self.goToHomeVC()
                    
                }
              
            }else{
                self.hideLoading()
                UserDefaults.standard.set(false, forKey: "isLogin")
                self.showAlertMessage(title: "Error", message: "User not found")
            }

            
        }
    }
    private func goToHomeVC(){
        let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "LGSideViewControllers")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    
    

}

extension LoginViewController:ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding{
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
    
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
//
        switch authorization.credential {
        
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            let userIdentifier = appleIDCredential.user
          
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
         
            let token = appleIDCredential.identityToken
            
            
        default:
            break
        }
        
        
        func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
            return self.view.window!
        }
        
        
    }
}


func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
    // Handle error.
    
    
    print("Sign in with Apple errored: \(error)")
    

    
}

extension LoginViewController{
    
    
    func loginSocial(email:String,name:String,img:String,provider:String,idToken:String){
        
        UserApiController.loginBySocial(name: name ?? "", email: email ?? "", profile: img, provider: provider, token: idToken) { (status, msg,Token)  in
            if status{
                self.hideLoading()
                UserDefaults.standard.setValue("Bearer \(Token)", forKey: "Token")
                let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "LGSideViewControllers")
                self.navigationController?.pushViewController(vc, animated: true)
                
                
            }else{
                self.hideLoading()
                self.showAlertMessage(title: "Error", message: msg)
            }
        }
    }
    
    //MARK:- Facebook Login
    func loginByFB(){
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { [self] (loginResult) in
            
            switch loginResult{
            case .failed(let error):
                
                break
            //                self.showAlert(title: "", message: error.localizedDescription)
            
            case .cancelled:
                //               self.showAlert(title: "", message: "User canceled login process!")
                break
            case .success(granted: _, declined: _, token: _):
                
                self.getFBUserData()
                
                
            }
        }
    }
    
    func getFBUserData() {
        //which if my function to get facebook user details
        if((AccessToken.current) != nil){
            print("FBToken111 \(String(describing: AccessToken.current))")
            
            let tokenFB = "\(String(describing: AccessToken.current))"
            // UserDefaults.standard.setAccessToken(tokenFB: "FBToken")
            
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, gender"]).start(completionHandler: { [self] (connection, result, error) -> Void in
                if (error == nil){
                    
                    let dict = result as! [String : AnyObject]
                    print(result!)
                    print(dict)
                    
                    
                    let picutreDic = dict as NSDictionary
                    let tmpURL1 = picutreDic.object(forKey: "picture") as! NSDictionary
                    let tmpURL2 = tmpURL1.object(forKey: "data") as! NSDictionary
                   let pic = tmpURL2.object(forKey: "url") as! String
                    
                    let email = picutreDic.object(forKey: "email") as! String
                    let nameOfUser = picutreDic.object(forKey: "name") as! String
                    let idOfUser = picutreDic.object(forKey: "id") as! String
                    print("email \(email)")
                    
                    
                    let tokenFB = "\(String(describing: AccessToken.current))"

                    loginSocial(email: email, name:nameOfUser , img: pic, provider: "facebook", idToken: tokenFB)
                    
                    var tmpEmailAdd = ""
                    if let emailAddress = picutreDic.object(forKey: "email") {
                        tmpEmailAdd = emailAddress as! String
                    }
                    else {
                        var usrName = nameOfUser
                        usrName = usrName.replacingOccurrences(of: " ", with: "")
                        tmpEmailAdd = usrName+"@facebook.com"
                    }
                    
                    // PLEASE SUBSCRIBE MY CHANNEL IT WILL MOTIVATE ME KEEP UPLOAD VIDEOS ;)
                    
                }
                
                print("ERROR FB LOGIN", error?.localizedDescription as Any)
            })
        }
    }
    
    
    
    
    
    
}

