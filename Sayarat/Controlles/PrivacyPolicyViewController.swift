//
//  PrivacyPolicyViewController.swift
//  Sayarat
//
//  Created by macbook on 3/14/21.
//

import UIKit
import WebKit

class PrivacyPolicyViewController: UIViewController {
    @IBOutlet var webView: WKWebView!
    var url = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        privacyPolicy()
        navigationController?.navigationBar.isHidden = false
        navigationItem.title = NSLocalizedString("Privacy Policy".localized, comment: "")
    
            
        
    }
    


}
extension PrivacyPolicyViewController{
    
    func privacyPolicy(){
        internetConnectionChecker { (status) in
            if status{
                self.showLoading()
        MenuApiController.privacyPolicy { (status, message) in
            self.url=message
            let urlvideoo = URL(string:self.url)
        let request = URLRequest(url: urlvideoo!)
            self.hideLoading()
            self.webView.load(request)
            self.webView.scrollView.isScrollEnabled=true
        }
        
    
            }else{
                self.showAlertMessage(title: "Error", message: "No internet connection")

            }
}
    }
}
