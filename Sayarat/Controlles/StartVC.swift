//
//  StartVC.swift
//  Sayarat
//
//  Created by Ahmed Issawi on 27/01/2022.
//

import UIKit

class StartVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        start()
        // Do any additional setup after loading the view.
    }
    
    func start(){
        
        DispatchQueue.main.async {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.checkUser()
            })
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func checkUser(){
        let isLoign = UserDefaults.standard.bool(forKey: "isLogin")
        if isLoign{
            let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "LGSideViewControllers")
            let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            window?.rootViewController = vc
            window?.makeKeyAndVisible()
        }else{
            let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "navigate")
            let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            window?.rootViewController = vc
            window?.makeKeyAndVisible()


        }
    }

}
