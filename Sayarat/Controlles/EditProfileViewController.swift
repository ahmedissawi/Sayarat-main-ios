//
//  EditProfileViewController.swift
//  Sayarat
//
//  Created by macbook on 3/14/21.
//

import UIKit
import CountryPickerView
import Photos
import MobileCoreServices
import SDWebImage
class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate,MapCoordinatesDelgate {
    
    func SelectedDone(Lat: Double, Lon: Double, address: String) {
        self.lat = Lat
        self.lon = Lon
        self.address.text = address
    }
    
    
    
    var cpv = CountryPickerView()
    var allNumber:String = ""
    private var uid: String = ""
    var urlphoto:String? = nil
    var userUrl:URL?
    var purl=""
    var selectedImage:UIImage?

    var profileData:Profile?
    @IBOutlet var change: UIButton!
    var lon:Double = 0.0
    var lat:Double = 0.0
    var addressMap:String = ""

    @IBOutlet var userImage: UIImageViewDesignable!
    @IBOutlet var fullNametxt: BottomBorderTextField!
    @IBOutlet var emailtxt: BottomBorderTextField!
    @IBOutlet var address: BottomBorderTextField!
    @IBOutlet var password: BottomBorderTextField!

    @IBOutlet var phoneNum: BottomBorderTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = false
        navigationItem.title = "Edit Profile".localized
        self.getDataProfile()
//        if UserDefaults.standard.integer(forKey: "way" ) == 1{
//            change.isEnabled=true
//
//        }else{
//        change.isEnabled=false
//        }
    }
   
    
    @IBAction func chooseImage(_ sender: Any) {
        let picker = UIImagePickerController()
                picker.sourceType = .photoLibrary
                picker.delegate = self
                picker.allowsEditing = true
                present(picker,animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {return  }
        print(image)
         selectedImage=image
        self.userImage.image = selectedImage
        guard let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL else { return }
          print(fileUrl.lastPathComponent)

        
    }
    
    
    @IBAction func didTabMap(_ sender: Any) {
        let vc = MapVC()
        vc.MapDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
    

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changePassword(_ sender: Any) {
        let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "ChangePasswordViewController")
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func codeNum(_ sender: Any) {
        cpv.showCountriesList(from: self)

    }
    
    @IBAction func saveAction(_ sender: Any) {
        internetConnectionChecker { (status) in
            if self.checkData(){
                self.editData()
            }
        }
       
        
    }
}

extension EditProfileViewController{
    public func checkData()-> Bool {
        
        if !fullNametxt.text!.isEmpty  && !address.text!.isEmpty && !phoneNum.text!.isEmpty {
            return true
        }else{
            showAlertMessage(title: "Error", message: "Please enter  All required data")
            
        }
        return false
    }
    

    public func getDataProfile(){
        internetConnectionChecker { (status) in
            if status{
                self.showLoading()
        MenuApiController.GetProfile { (status, user) in
            if status{
                
                self.hideLoading()
            self.userImage.sd_setImage(with: URL(string: user?.image_profile ?? ""))
            self.fullNametxt.text = user?.name
            self.address.text = user?.address
            self.emailtxt.text = user?.email
            self.phoneNum.text = user?.mobile

                
        }
            self.hideLoading()

        }
            }else{
                self.showAlertMessage(title: "Error", message: "No internet connection")
            }
        }
    }
    private func goToHomeVC(){
        let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "LGSideViewControllers")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    public func editData(){
        internetConnectionChecker { (status) in
            if status{
                self.showLoading()
                self.profileData = Profile(name: self.fullNametxt.text ?? "", mobile: self.phoneNum.text ?? "", profileImg: self.userImage.image!, address: self.address.text ?? "", email: self.emailtxt.text ?? "",latitude: self.lat,longitude: self.lon)
                guard  let _user = self.profileData else {return}
        MenuApiController.EditProfile(user: _user) { (status, message) in
            
            self.hideLoading()
            
                self.showAlertMessage(title: "Sayarat", message: message)
            
                  }
        
        
            }else{
                self.showAlertMessage(title: "Error", message: "No internet connection")

            }
    
        }
    }
}
