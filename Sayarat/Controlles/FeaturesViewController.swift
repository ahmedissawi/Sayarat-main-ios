//
//  FeaturesViewController.swift
//  Sayarat
//
//  Created by macbook on 3/12/21.
//

import UIKit

class FeaturesViewController: UIViewController {
    var fearure:[String] = []
    

    @IBOutlet var displayTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        fearure = UserDefaults.standard.stringArray(forKey: "feature") ?? []
        navigationController?.navigationBar.isHidden = false
        navigationItem.title = "Features".localized
        registerNib()
        displayTable.delegate=self
        displayTable.dataSource=self
        // Do any additional setup after loading the view.
    }
    func registerNib(){
        let nib = UINib(nibName: "CarFeatureTableViewCell", bundle: nil)
        displayTable.register(nib, forCellReuseIdentifier: "CarFeatureTableViewCell")
    }

}
extension FeaturesViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fearure.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarFeatureTableViewCell", for: indexPath) as! CarFeatureTableViewCell
        cell.feature.text=fearure[indexPath.row]
        cell.selectionStyle = .none

        return cell
    }
      
    
}

