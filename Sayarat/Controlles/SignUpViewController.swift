//
//  SignUpViewController.swift
//  Sayarat
//
//  Created by macbook on 3/14/21.
//

import UIKit
import CountryPickerView
import BSImagePicker
import Photos
import MobileCoreServices
import ObjectMapper


class SignUpViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,MapCoordinatesDelgate{
    
    
    func SelectedDone(Lat: Double, Lon: Double, address: String) {
        self.lat = Lat
        self.lon = Lon
        self.address.text = address
    }
    
    
    
    
    var allNumber:String = ""
    var imgName:String=""
    var urlphoto = ""
    var test :Bool=false
    let imagePicker = ImagePickerController()
    var user :UserData?
//    private let storage = Storage.storage().reference()
    var selectedImage:UIImage?
    var cpv = CountryPickerView()
    @IBOutlet var codeNum: UILabel!
    @IBOutlet var img: UIImageView!
    @IBOutlet var fullNametxt: BottomBorderTextField!
    @IBOutlet var emailTxt: BottomBorderTextField!
    
    @IBOutlet var PasswordTxt: BottomBorderTextField!
    @IBOutlet var confirmPassword: BottomBorderTextField!
    
    @IBOutlet var phoneNum: BottomBorderTextField!
    @IBOutlet var address: BottomBorderTextField!
    var lon:Double = 0.0
    var lat:Double = 0.0
    var addressMap:String = ""
    @IBOutlet var userImage: UIImageViewDesignable!
    override func viewDidLoad() {
        super.viewDidLoad()
        setCountryPickerView()
//        userImage.contentMode = .scaleAspectFit


    }
    
    
    
    @IBAction func didTabMap(_ sender: Any) {
        let vc = MapVC()
        vc.MapDelegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    func setCountryPickerView() {
        cpv = CountryPickerView(frame:CGRect(x: 0, y: 0, width: 120, height: 20))
        cpv.delegate = self
        cpv.dataSource = self
        img.image = cpv.selectedCountry.flag
        codeNum.text = cpv.selectedCountry.phoneCode
    }
    @IBAction func signUp(_ sender: Any) {
        internetConnectionChecker { (status) in
            if self.checkData(){
                self.signUp()
            }
        }
    }
    
 
    
    //            UserDefaults.standard.setValue(urlString, forKey: "url")

    @IBAction func login(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    @IBAction func codeNum(_ sender: Any) {
        cpv.showCountriesList(from: self)
    }
    
    @IBAction func pickImage(_ sender: Any) {
let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.allowsEditing = true
        present(picker,animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {return  }
        print(image)
        selectedImage = image
        self.userImage.image = selectedImage
        guard let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL else { return }

        
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    
}
extension SignUpViewController: CountryPickerViewDelegate, CountryPickerViewDataSource{
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        //        country.name
        codeNum.text = country.phoneCode
        img.image = country.flag
        allNumber = "\(codeNum.text!)"
        
    }
   
    
}

extension SignUpViewController {
    public func checkData()-> Bool {

        if !emailTxt.text!.isEmpty && !phoneNum.text!.isEmpty && !fullNametxt.text!.isEmpty && !PasswordTxt.text!.isEmpty && !confirmPassword.text!.isEmpty {
            return true
            
        }else{
            showAlertMessage(title: "Error", message: "Please enter  All required data")
            return false
            
        }
        
    }
    
    
    private func signUp(){
        guard selectedImage != nil else{
            self.showAlertMessage(title: "Error".localized, message: "Please choose a picture".localized)
            return
        }
        internetConnectionChecker { (status) in
            if status{
                let phone = "\(self.codeNum.text ?? "")" + "\(self.phoneNum.text ?? "")"
                self.user = UserData(name: self.fullNametxt.text!, mobile: phone, profileImg: self.selectedImage!, address: self.address.text ?? "", password: self.PasswordTxt.text!, confirmPassword: self.confirmPassword.text!, email: self.emailTxt.text!,latitude:self.lat,longitude:self.lon)
                guard let _user = self.user else {return}
                self.showLoading()
                UserApiController.register(user: _user) { (status,message) in
                                    if status == true{
                                        self.showOkAlert(withTitle: "Sayarat".localized, message: message) {
                                            self.hideLoading()
                                            self.clear ()
                                            self.dismiss(animated: true, completion: nil)
                                        }
                                    }else{
                                        self.hideLoading()
                                        self.showAlertMessage(title: "Sayarat", message: message)

                                    }
                             }

            }else{
                self.showAlertMessage(title: "Error".localized, message: "No internet connection".localized)
            }
        }
    }
    
    
    
    func clear (){
        emailTxt.text = ""
        PasswordTxt.text = ""
        fullNametxt.text = ""
        PasswordTxt.text = ""
        confirmPassword.text = ""
        address.text = ""
        
    }

}

extension UIViewController{
func showOkAlert(withTitle title: String, message : String, buttonTitle: String = "OK".localized,  style: UIAlertController.Style? = .alert, okayButtonCompletionHandler: (() -> Void)? = nil) {
    
   // Utility.vibrateFeedBack()
    let alertController = UIAlertController(title: title, message: message, preferredStyle: style!)
    let OKAction = UIAlertAction(title: buttonTitle, style: .default) { action in
        guard let completionHandler = okayButtonCompletionHandler?() else { return }
        completionHandler
    }
    
   
    alertController.addAction(OKAction)
    
    if style == UIAlertController.Style.actionSheet {
        let cancelButton = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(cancelButton)
    }
    DispatchQueue.main.async {
        self.present(alertController, animated: true, completion: nil)
    }
}
}
