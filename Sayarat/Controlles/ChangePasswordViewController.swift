//
//  ChangePasswordViewController.swift
//  Sayarat
//
//  Created by macbook on 3/14/21.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    private var uid: String = ""
    var opass=""
    @IBOutlet var oldPassword: BottomBorderTextField!
    @IBOutlet var newPassword: BottomBorderTextField!
    @IBOutlet var confirmPassword: BottomBorderTextField!
    var iconClick = true

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isHidden = false
        navigationItem.title = NSLocalizedString("Change password".localized, comment: "")
    }
    
    @IBAction func showP(_ sender: Any) {
        if(iconClick == true) {
            oldPassword.isSecureTextEntry = false
          } else {
            oldPassword.isSecureTextEntry = true
          }

          iconClick = !iconClick
    }
    
    
    @IBAction func showN(_ sender: Any) {
        if(iconClick == true) {
            newPassword.isSecureTextEntry = false
          } else {
            newPassword.isSecureTextEntry = true
          }

          iconClick = !iconClick

    }
    
    
    
    @IBAction func showConfirm(_ sender: Any) {
        if(iconClick == true) {
            confirmPassword.isSecureTextEntry = false
          } else {
            confirmPassword.isSecureTextEntry = true
          }

          iconClick = !iconClick

    }
    
    @IBAction func change(_ sender: Any) {
        
        internetConnectionChecker { (status) in
            if self.checkData(){
                self.changePassword()
            }
        }
        dismiss(animated: true, completion: nil)
    }
    

}
extension ChangePasswordViewController{
    public func checkData()-> Bool {
        
        if !oldPassword.text!.isEmpty && !newPassword.text!.isEmpty && !confirmPassword.text!.isEmpty{
            return true
        }
        return false
    }
    
    func changePassword(){

        internetConnectionChecker { (status) in
            if status{
                self.showLoading()
                MenuApiController.changePassword(oldPass: self.oldPassword.text!, newPass: self.newPassword.text!, confirmPass: self.confirmPassword.text!) { (status, message) in
                self.clearData()
                    self.hideLoading()

                self.showAlertMessage(title: "Sayarat", message: message)
                
        }
            }else{
                self.showAlertMessage(title: "Error", message: "No internet connection")

            }
        }
}
    
    
    func clearData(){

        oldPassword.text = ""
        confirmPassword.text = ""
        newPassword.text = ""
         
    }
    
}
