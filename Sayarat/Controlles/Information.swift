//
//  Information.swift
//  Sayarat
//
//  Created by macbook on 3/13/21.
//

import Foundation

class Information{
    var isCollapsed:Bool?
    public var labelName:String?


    init(labelName:String,Collapsed:Bool) {
        self.labelName=labelName
        self.isCollapsed=Collapsed
        
    }
}
enum MainInfoCell{
    case Car_Images
    case Car_Details
    case Basic_info
    case Description
    case Features
    case Location


    
    var labelName:String{

        switch self {
        case .Car_Images:

            return "Car Images"
        case .Car_Details:
  

            return "Car Details"
        case .Basic_info:

            return "Basic info"
        case .Description:

            return "Description"
        case .Features:

            return "Features"
        case .Location:

            return "Location"
   
        }
    }
    
    var isCollapsed:Bool{
    

        switch self {
        case .Car_Images:
            return true

        case .Car_Details:
            return true

        case .Basic_info:
            return true
        case .Description:
            return true
        case .Features:
            return true
        case .Location:
            return true
   
        }
    }
}
