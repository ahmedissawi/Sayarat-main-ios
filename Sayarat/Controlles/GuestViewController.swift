//
//  GuestViewController.swift
//  Sayarat
//
//  Created by macbook on 4/25/21.
//

import UIKit
import GoogleMobileAds
class GuestViewController: UIViewController {
    @IBOutlet var bannerAds: GADBannerView!
    
    var owner:[String]=[]
    var user:[Datas]=[]
    var similarCar:[Similar_cars]=[]
    var car:Car?=nil
   var carSname:[String]=[]
    var carSprice:[String]=[]
    var CarDatad:[String]=[]
    
    var fullName:[String] = []
    
    var userC:[String]=[]
    var Automation:[String]=[]
    var saloon:[String]=[]
    var model:[String]=[]
    var fuel:[String]=[]
    var userName:[String]=[]
    
    var millege:[String]=[]
    var price:[String]=[]
    var documentid:[String]=[]
    var userid:[String]=[]
    var photos:[String]=[]
    var address:[String]=[]
    var username:[String]=[]
    var carName:[String]=[]
    var feature:[String]=[]
    var img:[String]=[]
    var userUrl:URL?
    @IBOutlet var displayTable: UITableView!
    
    @IBOutlet var allC: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerAds.adUnitID = "ca-app-pub-7967951980951384/4828209712"
        bannerAds.load(GADRequest())
        bannerAds.rootViewController=self


        navigationController?.navigationBar.isHidden = false
        navigationItem.title =  NSLocalizedString("Home".localized, comment: "")

        getHomeData()
        registerNib()
        displayTable.delegate=self
        displayTable.dataSource=self
        allC.font = UIFont(name: "Ubuntu", size: 16)
    }
    
    func registerNib(){
        let nib = UINib(nibName: "HomesTableViewCell", bundle: nil)
        displayTable.register(nib, forCellReuseIdentifier: "HomesTableViewCell")
    }
  
    //        self.showAlertMessage(title: "Sayarat", message: "To sell your car you should create a personal account")

    @IBAction func sellYourCar(_ sender: Any) {
        showAlertPopUp(title: "Sayarat", message: "To sell your car you should create a personal account".localized) {
            let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "LoginViewController")
            self.navigationController?.pushViewController(vc, animated: true)
        } action2: {
            
        }

     
    }
    


}

    extension GuestViewController : UITableViewDataSource,UITableViewDelegate{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return user.count
            
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomesTableViewCell", for: indexPath) as! HomesTableViewCell
            cell.configureHome(homeCar: user[indexPath.row])
            cell.selectionStyle = .none

            return cell
        }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          
            carDetails(id: user[indexPath.row].id ?? 0)

        }

        
}

extension GuestViewController{
    
    func getHomeData(){
        internetConnectionChecker { (statuss) in
            if statuss{
                self.showLoading()
        UserApiController.HomeData { (status, user) in
            if status {
                self.user = user ?? []
                self.hideLoading()

                self.displayTable.reloadData()
            }else{
                self.hideLoading()

                self.showAlertMessage(title: "Error".localized, message: "No data".localized)

            }
           
            
            
        }
            }else{
                self.showAlertMessage(title: "Error".localized, message: "No internet connection".localized)

            }
    
        }
    }
    
    func carDetails(id:Int){
        internetConnectionChecker { (statuss) in
            if statuss{
        CarApiController.carDetails(id: id) { (status, carData, similarCar) in
            if status{
                self.car = carData
                self.similarCar = similarCar ?? []
                self.showData()
                if  self.similarCar.count != 0 {
                    for index in 0 ... self.similarCar.count-1{
                        
                        self.Automation.append( self.similarCar[index].engine?.name ?? "")
                        self.saloon.append( self.similarCar[index].saloon?.name ?? "")
                        self.fuel.append( self.similarCar[index].fuel?.name ?? "")
                        self.millege.append(self.similarCar[index].mileage ?? "")
                        self.carSname.append(self.similarCar[index].name ?? "")
                        self.carSprice.append("\(self.similarCar[index].price ?? 0)")
                        self.photos.append(self.similarCar[index].photos?[0] ?? "")
                        
                    }
                }
           
                if self.car?.features!.count != 0 {
                    for index in 0 ... ((self.car?.features!.count)!-1){
                        self.feature.append(self.car?.features?[index].name ?? "")
                        
                    }
                }
               
        
                
               
                UserDefaults.standard.set(self.car?.owner?.name, forKey: "ownerName")
                UserDefaults.standard.set(self.car?.owner?.email, forKey: "ownerEmail")
                UserDefaults.standard.set(self.car?.owner?.mobile, forKey: "ownerMobile")
                UserDefaults.standard.set(self.car?.owner?.address, forKey: "ownerAddress")
                UserDefaults.standard.set(self.car?.mileage, forKey: "carmillege")
                UserDefaults.standard.set(self.car?.fuel?.name, forKey: "carfuel")
                UserDefaults.standard.set(self.car?.saloon?.name, forKey: "carsaloon")
                UserDefaults.standard.set(self.car?.engine?.name, forKey: "carengine")
                UserDefaults.standard.set(self.car?.mileage, forKey: "carmillege")
                UserDefaults.standard.set(self.car?.saloon?.name, forKey: "carsaloon")
                UserDefaults.standard.set(self.car?.name, forKey: "catdatan")
                UserDefaults.standard.set(self.car?.price, forKey: "cardatap")
                UserDefaults.standard.set(self.car?.photos?[0], forKey: "cardatapic")






                UserDefaults.standard.set(self.Automation, forKey: "Automation")
                UserDefaults.standard.set(self.saloon, forKey: "saloon")
                UserDefaults.standard.set(self.fuel, forKey: "fuel")
                UserDefaults.standard.set(self.millege, forKey: "millege")
                UserDefaults.standard.set(self.carSname, forKey: "carSname")
                UserDefaults.standard.set(self.carSprice, forKey: "carSprice")
                UserDefaults.standard.set(self.feature, forKey: "feature")
                UserDefaults.standard.set(self.photos, forKey: "carphoto")

                UserDefaults.standard.set(self.car?.longitude, forKey: "longitude")
                UserDefaults.standard.set(self.car?.latitude, forKey: "latitude")
                UserDefaults.standard.set(self.car?.details, forKey: "description")

           

                let vc=UIStoryboard.mainStoryBoard.instantiateViewController(withIdentifier: "PreviewUserInfoViewController")  as! PreviewUserInfoViewController
                vc.carDetails=self.car
           
                self.navigationController?.pushViewController(vc, animated: true)
            }
         
            
        }
            }else{
                self.showAlertMessage(title: "Error", message: "No internet connection")
            }
        }
    }
    
    func showData(){
        CarDatad=[]
        CarDatad.append(self.car?.mileage ?? "")
        CarDatad.append(self.car?.saloon?.name ?? "")
        CarDatad.append(self.car?.year ?? "")
        CarDatad.append(self.car?.fuel?.name ?? "")
        CarDatad.append(self.car?.engine?.name ?? "")
        CarDatad.append(self.car?.color ?? "")
        CarDatad.append(self.car?.service_history?.name ?? "")
      UserDefaults.standard.set(CarDatad,forKey: "carbasic")


    }

}

