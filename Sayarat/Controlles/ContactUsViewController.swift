//
//  ContactUsViewController.swift
//  Sayarat
//
//  Created by macbook on 3/14/21.
//

import UIKit

class ContactUsViewController: UIViewController {
    
    @IBOutlet var nametxt: BottomBorderTextField!
    @IBOutlet var emailtxt: BottomBorderTextField!
    @IBOutlet var subject: BottomBorderTextField!
    @IBOutlet var messege: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = false
        navigationItem.title = NSLocalizedString("Contact Us".localized, comment: "")
        
        
    }
    
    
    @IBAction func upload(_ sender: Any) {
    }
    
    
    @IBAction func send(_ sender: Any) {
        
        internetConnectionChecker(callback: { (status) in
            if status{
                if self.checkData(){
                    self.contactUs()
                }
            }
        })
    }
}
extension ContactUsViewController {
    
    public func checkData()-> Bool {
        
        if !nametxt.text!.isEmpty && !emailtxt.text!.isEmpty && !subject.text!.isEmpty && !messege.text!.isEmpty{
            return true
            
            
        }else{
            showAlertMessage(title: "Error", message: "Please enter  All required data")
            return false
            
        }
        
    }
    
    
    func clear (){
        nametxt.text = ""
        emailtxt.text = ""
        subject.text = ""
        messege.text = ""
        
        
        
    }
    
    func contactUs(){
        internetConnectionChecker { (status) in
            if status{
                self.showLoading()
                MenuApiController.ContactUs(title: self.subject.text!, message: self.messege.text!) { (status, message) in
                self.clear()
                    self.hideLoading()
                self.showAlertMessage(title: "Sayarat App", message: message)
        }
        
            }else{
                self.showAlertMessage(title: "Error", message: "No internet connection")

            }
            }
    }
    
}



