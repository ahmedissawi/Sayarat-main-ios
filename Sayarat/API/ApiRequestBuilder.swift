//
//  MenuApiController.swift
//  Sayarat
//
//  Created by macbook on 8/24/21.
//
//import Foundation
//import ObjectMapper
//
//class ApiRequestBuilder: BaseApiController {
//
//    typealias APIRequestResponse<T> = (_ status: Bool, _ object: T?) -> Void
//
//    private static var shared: ApiRequestBuilder?
//    private override init() {
//        //MUST BE PRIVATE TO DISABLE USER CALL AND ACCESS ONLY BY getInstance()
//    }
//
//    private var url: String?
//    private var method: HTTPMethod = .get
//    private var parameters: Parameters = Parameters()
//    private var showMessage: Bool = true
//    private var param: HTTPHeaders = HTTPHeaders()
//
//    static func getInstance()-> ApiRequestBuilder?{
//        if let _shared = shared {
//            return _shared
//        }
//        shared = ApiRequestBuilder()
//        return shared
//    }
//
//    func url(url: String) -> ApiRequestBuilder {
//        self.url = url
//        return self
//    }
//
//    func method(method: HTTPMethod) -> ApiRequestBuilder {
//        self.method = method
//        return self
//    }
//
//    func set(key: String, value: Any) -> ApiRequestBuilder {
//        parameters[key] = value
//        return self
//    }
//    func putHeader(key: String, value: Any) -> ApiRequestBuilder {
//        param[key] = value as? String
//        return self
//    }
//    func enableMessage(status: Bool)->ApiRequestBuilder{
//        self.showMessage = status
//        return self
//    }
//
//    public func build<T: BaseResponse>(callback: @escaping APIRequestResponse<T>){
//        internetConnectionChecker { (status) in
//            if status{
//                self.showLoading()
//                Alamofire.request(self.url!, method: self.method, parameters: self.parameters, headers: self.param).responseObject { (response: DataResponse<T>) in
//
//                    if let _baseResponse = response.result.value {
//                        let status = _baseResponse.status ?? false
//                        let message = _baseResponse.message ?? ""
//                        if status{
//                            print(_baseResponse)
//                            callback(true, _baseResponse)
//
//
//                    }else{
//                        callback(false, nil)
//                    }
//                    self.hideLoading()
//                    self.reset()
//                }
//            }
//        }
//    }
//    }
//
//
//
//    private func reset(){
//        url =  ""
//        method = .get
//        parameters = Parameters()
//        showMessage = true
//    }
//}
