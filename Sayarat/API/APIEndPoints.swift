//
//  MenuApiController.swift
//  Sayarat
//
//  Created by macbook on 8/24/21.
//
import Foundation

enum API_KEYS: String {
    
    case BASE_URL = "http://sayarat.shop/api/"
    case USER_REGISTER = "signUpUsers"
    case USER_LOGIN = "loginForUsers"
    case USER_LOGUT = "logout"
    case changePassword = "changePassword"
    case aboutUs = "aboutUs"
    case setting = "settings"
    case resetPassword =  "forgotPassword"
    case contactUs =  "sendContactUsMsg"
    case profiledata = "profile"
    case editProfile = "editProfile"
    case getMyCar = "getMyCars"
    case homeGet = "homeScreen"
    case countries = "getCountries"
    case sellUcar = "sellYourCar"
    case carDetails = "getCarDetails"
    case loginSocial = "loginBySocial"
    
    
    var url: String {
        switch self {
        case .BASE_URL:
            return API_KEYS.BASE_URL.rawValue
            
        default:
            return "\(API_KEYS.BASE_URL.rawValue)\(self.rawValue)"
        }
    }
    
    func withId(id: Int) -> String {
        switch self {
        case .carDetails:
            return "\(API_KEYS.BASE_URL.rawValue)\(self.rawValue)?id=\(id)"
 
        default:
            return ""
        }
    }
    
}
