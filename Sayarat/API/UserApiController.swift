//
//  MenuApiController.swift
//  Sayarat
//
//  Created by macbook on 8/24/21.
//
import Foundation
import ObjectMapper
import Alamofire
typealias Callback = ( _ status: Bool ,_ message:String) -> Void

struct UserApiController{
    
    static func register(user: UserData, callback: @escaping Callback){
 
                let imgData = user.profileImg.jpegData(compressionQuality: 0.2)!
       

                let parameters: Parameters = ["name":user.name,
                                              "email":user.email,
                                              "mobile":user.mobile,
                                              "address":user.address,
                                              "password":user.password,
                                              "latitude":user.latitude,
                                              "longitude":user.longitude,
                                              "confirm_password":user.confirmPassword,
                                              "device_type":"ios"
                ]
                

                AF.upload(multipartFormData: { multipartFormData in
                    multipartFormData.append(imgData, withName: "image_profile",fileName: "image_\(Date().toMillis() ?? 0).jpeg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)                } //Optional for extra parameters
                    
                },
                to:API_KEYS.USER_REGISTER.url, method: .post,headers: ["Accept-Language": "en"]).validate().responseJSON { [self] (response) in
               if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
             print(str)
                   print(data)
                        if let parsedMapperString : SignUp = Mapper<SignUp>().map(JSONString:str){
                            if parsedMapperString.status == true{
                                
                                callback(true, parsedMapperString.message!)
                            }else{
                                callback(false, parsedMapperString.message!)

                                print("user Not found")
                            }
                            
                  }
                        }
                    }
            }


    static func Login(email: String,password:String, callback: @escaping (Userlogin?,Bool) ->Void){


            let parameters: Parameters = ["email":email,
                                          "password":password]
            

        AF.request(API_KEYS.USER_LOGIN.url, method: .post , parameters:parameters,headers:["Accept-Language": "en"]).validate().responseJSON { [self] (response) in
           if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
         print(str)
               print(data)
                    if let parsedMapperString : Login = Mapper<Login>().map(JSONString:str){
                        if parsedMapperString.status == true{
                            callback(parsedMapperString.user!, true)
                        }else{
                            callback( nil , false)

                            print("user Not found")
                        }
                        
              }
                    }
                }
        }


    static func resetPassword(email: String, callback: @escaping Callback){


            let parameters: Parameters = ["email":email]
            

        AF.request(API_KEYS.resetPassword.url, method: .post , parameters:parameters,headers:["Accept-Language": "en"]).validate().responseJSON { [self] (response) in
           if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
         print(str)
               print(data)
                    if let parsedMapperString : Response = Mapper<Response>().map(JSONString:str){
                        if parsedMapperString.status == true{
                            callback(true, parsedMapperString.message ?? "")
                        }else{
                            callback(true, parsedMapperString.message ?? "")

                            print("user Not found")
                        }
                        
              }
                    }
                }
        }
    
    
    static func HomeData( callback: @escaping (Bool,[Datas]?) ->Void){


        AF.request(API_KEYS.homeGet.url, method: .get , parameters:nil,headers:["Accept-Language": "en"]).validate().responseJSON { [self] (response) in
           if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
         print(str)
               print(data)
                    if let parsedMapperString : HomeData = Mapper<HomeData>().map(JSONString:str){
                        if parsedMapperString.status == true{
                            callback(true,parsedMapperString.cars?.data)
                        }else{
                            callback(false,nil)

                            print("user Not found")
                        }
                        
              }
                    }
                }
        }
    static func FilterData(data:FilterData, callback: @escaping (Bool,[Datas]?) ->Void){
     let  url = "http://sayarat.muthqaf.net/public/api/homeScreen?car_type_id=\(data.car_type_id)&payment_method=\(data.payment_method)&country_id=\(data.country_id)&price_from=\(data.price_from)&price_to=\(data.price_to)"

        AF.request(url, method: .get , parameters:nil,headers:["Accept-Language": "en"]).validate().responseJSON {  (response) in
           if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
         print(str)
               print(data)
                    if let parsedMapperString : HomeData = Mapper<HomeData>().map(JSONString:str){
                        if parsedMapperString.status == true{
                            callback(true,parsedMapperString.cars?.data)
                        }else{
                            callback(false,nil)

                            print("user Not found")
                        }
                        
              }
                    }
                }
        }
    
    static func loginBySocial(name:String,email:String,profile:String,provider:String,token:String, callback: @escaping (Bool,String,String) ->Void){
        
        let parameters: Parameters = ["name":name,
                                      "email":email,
                                      "social_img":profile,
                                      "provider":provider,
                                      "social_token":token,
                                      "device_type":"ios"
        ]
        

        AF.request(API_KEYS.loginSocial.url, method: .post , parameters:parameters,headers:["Accept-Language": "en"]).validate().responseJSON { [self] (response) in
           if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
         print(str)
               print(data)
                    if let parsedMapperString : profilesData = Mapper<profilesData>().map(JSONString:str){
                        if parsedMapperString.status == true{
                            callback(true,parsedMapperString.message ?? "", parsedMapperString.user?.access_token ?? "")
                        }else{
                            callback(false,parsedMapperString.message ?? "",  parsedMapperString.user?.access_token ?? "")

                            print("user Not found")
                        }
                        
              }
                    }
                }
        }

       
    
//                Alamofire.request(API_KEYS.USER_REGISTER.url, method: .post, parameters: parameters).responseObject { (response: DataResponse<BaseGenericReponse<User>>) in
//
//                    if let _baseResponse = response.result.value{
//                        let status: Bool = _baseResponse.status ?? false
//                        let message: String = _baseResponse.message ?? ""
//
//                        if status{
//
//                            print("SUCCESS MESSAGE:",message)
//
//                            callback(true, _baseResponse.object)
//                        }else{
//                            //SHOW ERROR MESSAGE
//                            //FIRE CLOSURE WITHOUT DATA
//                            print("ERROR MESSAGE:",message)
//                            callback(false, nil)
//                        }
//                    }else{
//
//                    }
//                    self.hideLoading()
//                }


        

//
//    public func login(email: String, password: String, callback: @escaping ObjectRequestCallback<User>){
//        let parameters: Parameters = ["email":email, "password": password]
//
//        Alamofire.request(API_KEYS.USER_LOGIN.url, method: .post, parameters: parameters).responseObject { (response: DataResponse<BaseGenericReponse<User>>) in
//
//            if let _baseResponse = response.result.value{
//                let status: Bool = _baseResponse.status ?? false
//                let message: String = _baseResponse.message ?? ""
//
//                if status{
//                    //SAVE USER DATA TO USER DEFAULTS
//                    //SHOW SUCCESS MESSAGE
//                    //FIRE CLOSURE WITH DATA
////                    UserSettings.init().save(user: _baseResponse.object!)
//                    print("SUCCESS MESSAGE:",message)
//                    callback(true, _baseResponse.object)
//                }else{
//                    //SHOW ERROR MESSAGE
//                    //FIRE CLOSURE WITHOUT DATA
//                    print("ERROR MESSAGE:",message)
//                    callback(false, nil)
//                }
//            }else{
//
//            }
//        }
//    }


}
extension Date {
    
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }

}
