//
//  MenuApiController.swift
//  Sayarat
//
//  Created by macbook on 8/24/21.


import Foundation
import ObjectMapper
import Alamofire

typealias menuCallback = ( _ status: Bool ) -> Void

struct MenuApiController{
    
    static func logout( callback: @escaping Callback){
 

        let parameters: Parameters? = nil
                

        AF.request(API_KEYS.USER_LOGUT.url, method: .get , parameters:parameters,headers:["Accept-Language": "en","Authorization":"\(UserDefaults.standard.value(forKey: "Token") ?? "")"]).validate().responseJSON { [self] (response) in
           if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
         print(str)
               print(data)
                    if let parsedMapperString : Response = Mapper<Response>().map(JSONString:str){
                        if parsedMapperString.status == true{
                            callback(true, parsedMapperString.message!)
                        }else{
                            callback(false,  parsedMapperString.message!)

                            print("user Not found")
                        }
                        
              }
                    }
                }
            }
    
    static func AboutUs( callback: @escaping Callback){
 

        let parameters: Parameters? = nil
                

        AF.request(API_KEYS.aboutUs.url, method: .get , parameters:parameters,headers:["Accept-Language": "en"]).validate().responseJSON { [self] (response) in
           if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
         print(str)
               print(data)
                    if let parsedMapperString : AboutData = Mapper<AboutData>().map(JSONString:str){
                        if parsedMapperString.status == true{
                            callback(true, (parsedMapperString.page?.description!)!)
                        }else{
                            callback(false, "theres no data")
                        }
                        
              }
            
                    }
                }
            }
    static func privacyPolicy( callback: @escaping Callback){
 

        let parameters: Parameters? = nil
                

        AF.request(API_KEYS.setting.url, method: .get , parameters:parameters,headers:["Accept-Language": "en"]).validate().responseJSON { [self] (response) in
           if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
         print(str)
               print(data)
                    if let parsedMapperString : UserSetting = Mapper<UserSetting>().map(JSONString:str){
                        if parsedMapperString.status == true{
                            callback(true, (parsedMapperString.settings?.privacy_policy_url ?? ""))
                        }else{
                            callback(false,  "theres no data")
                        }
                        
              }
            
                    }
                }
            }
    static func ContactUs(title:String,message:String, callback: @escaping Callback){
 

        let parameters: Parameters? = ["title":title,
                                       "message":message
                                  
         ]
            
        AF.request(API_KEYS.contactUs.url, method: .post , parameters:parameters,headers:["Accept-Language": "en","Authorization":"\(UserDefaults.standard.value(forKey: "Token") ?? "")"]).validate().responseJSON { [self] (response) in
           if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
         print(str)
               print(data)
                    if let parsedMapperString : Response = Mapper<Response>().map(JSONString:str){
                        if parsedMapperString.status == true{
                            callback(true, parsedMapperString.message ?? "")
                        }else{
                            callback(false, parsedMapperString.message ?? "")
                        }
                        
              }
            
                    }
                }
            }
    
    
    
    static func GetProfile( callback: @escaping ( _ status:Bool,UserProfile?) -> Void){
 print(UserDefaults.standard.value(forKey: "Token") ?? "")

        let parameters: Parameters? = nil
            
        AF.request(API_KEYS.profiledata.url, method: .get , parameters:parameters,headers:["Accept-Language": "en","Authorization":"\(UserDefaults.standard.value(forKey: "Token") ?? "")"]).validate().responseJSON { [self] (response) in
           if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
         print(str)
               print(data)
                    if let parsedMapperString : profilesData = Mapper<profilesData>().map(JSONString:str){
                        if parsedMapperString.status == true{
                            callback(true, parsedMapperString.user)
                        }else{
                            callback(false,nil)
                        }
                        
              }
            
                    }
                }
            }
    
    
    
    
    static func EditProfile(user: Profile, callback:@escaping Callback){
 
                let imgData = user.profileImg.jpegData(compressionQuality: 0.2)!

                let parameters: Parameters = ["name":user.name,
                                              "email":user.email,
                                              "mobile":user.mobile,
                                              "address":user.address,
                                              "latitude":user.latitude,
                                              "longitude":user.longitude,
                                              "device_type":"ios"]
                

                AF.upload(multipartFormData: { multipartFormData in
                    multipartFormData.append(imgData, withName: "image_profile",fileName: "image_\(Date().toMillis() ?? 0).jpeg", mimeType: "image/jpg")
                    for (key, value) in parameters {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)                } //Optional for extra parameters
                    
                },
                to:API_KEYS.editProfile.url, method: .post,headers: ["Accept-Language": "en","Authorization":"\(UserDefaults.standard.value(forKey: "Token") ?? "")"]).validate().responseJSON { [self] (response) in
               if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
             print(str)
                   print(data)
                        if let parsedMapperString : profilesData = Mapper<profilesData>().map(JSONString:str){
                            if parsedMapperString.status == true{
                                
                                callback(true, parsedMapperString.message ?? "")
                            }else{
                                callback(false,"Error")

                            }
                            
                  }
                        }
                    }
            }
    
    
    
    
    
    static func changePassword(oldPass: String,newPass:String,confirmPass:String, callback:@escaping Callback){
 

                let parameters: Parameters = ["old_password":oldPass,
                                              "password":newPass,
                                              "confirm_password":confirmPass
                ]
                
                AF.request(API_KEYS.changePassword.url, method: .post,parameters: parameters,headers: ["Accept-Language": "en","Authorization":"\(UserDefaults.standard.value(forKey: "Token") ?? "")"]).validate().responseJSON { [self] (response) in
               if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
             print(str)
                   print(data)
                        if let parsedMapperString : Response = Mapper<Response>().map(JSONString:str){
                            if parsedMapperString.status == true{
                                
                                callback(true, parsedMapperString.message ?? "")
                            }else{
                                callback(true, parsedMapperString.message ?? "")

                            }
                            
                  }
                        }
                    }
            }
    
    static func myCar( callback:@escaping (_ status:Bool,[Cars]?) -> Void){
 
                AF.request(API_KEYS.getMyCar.url, method: .get,parameters: nil,headers: ["Accept-Language": "en","Authorization":"\(UserDefaults.standard.value(forKey: "Token") ?? "")"]).validate().responseJSON { [self] (response) in
               if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
             print(str)
                   print(data)
                        if let parsedMapperString : MyCar = Mapper<MyCar>().map(JSONString:str){
                            if parsedMapperString.status == true{
                                
                                callback(true,parsedMapperString.cars)
                            }else{
                                callback(false, nil)

                            }
                            
                  }
                        }
                    }
            }
    
}
