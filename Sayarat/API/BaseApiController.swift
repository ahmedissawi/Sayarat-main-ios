//
//  MenuApiController.swift
//  Sayarat
//
//  Created by macbook on 8/24/21.
//
import Foundation
import UIKit
import SVProgressHUD
import ObjectMapper
import SCLAlertView
class BaseApiController {
    
//    public func getHeaders()->HTTPHeaders{
//        var headers: HTTPHeaders?
//        if UserDefaults.standard.value(forKey: "Token") != nil || UserDefaults.standard.string(forKey: "Token") != ""{
//             headers = ["Authorization": "\(UserDefaults.standard.string(forKey: "token"))", "Accept-Language":"en"]
//        }else{
//            headers = ["Accept-Language":"en"]
//        }
//        return headers!
//    }
    

    
    public func noInternetErrorMessage(callback: @escaping InternetConnectionChecker){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false,
            shouldAutoDismiss: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Try Again") {
            if MainController.isConnectedToInternet(){
                alertView.hideView()
                callback(true)
            }else{
                callback(false)
            }
        }
        alertView.showError("Internt Connection", subTitle: "Check your internet connection")
    }
    
    public func internetConnectionChecker(callback: @escaping InternetConnectionChecker){
        if MainController.isConnectedToInternet() {
            callback(true)
        }else{
            noInternetErrorMessage(callback: callback)
        }
    }
    
    public func showLoading(){
        SVProgressHUD.setForegroundColor(UIColor.gray)
        SVProgressHUD.setBackgroundColor(UIColor.white)
        SVProgressHUD.show()
    }

    public func hideLoading(){
        SVProgressHUD.dismiss()
    }
    
}
