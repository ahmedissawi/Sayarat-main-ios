//  CarApiController.swift
//  Sayarat
//  Created by macbook on 8/29/21.

import Foundation
import ObjectMapper
import Alamofire

struct CarApiController{
static func countries(callback:@escaping (_ status:Bool,[Countries]?) -> Void){

            AF.request(API_KEYS.countries.url, method: .get,parameters: nil,headers: ["Accept-Language": "en"]).validate().responseJSON { [self] (response) in
           if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
         print(str)
               print(data)
                    if let parsedMapperString : GetCountries = Mapper<GetCountries>().map(JSONString:str){
                        if parsedMapperString.status == true{
                            
                            callback(true,parsedMapperString.countries)
                        }else{
                            callback(false, nil)

                        }
                        
              }
                    }
                }
        }
    
    
    static func Setting(callback:@escaping (_ status:Bool,[Countries]?,[Engine]?,[Saloons]?,[Cars_types]?,[Fuels]?,[Services_histories]?) -> Void){

                AF.request(API_KEYS.setting.url, method: .get ,headers: ["Accept-Language": "en"]).validate().responseJSON { [self] (response) in
               if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
             print(str)
                   print(data)
                        if let parsedMapperString : UserSetting = Mapper<UserSetting>().map(JSONString:str){
                            if parsedMapperString.status == true{
                                
                                callback(true,parsedMapperString.settings?.countries,parsedMapperString.settings?.engines,parsedMapperString.settings?.saloons,parsedMapperString.settings?.cars_types,parsedMapperString.settings?.fuels,parsedMapperString.settings?.services_histories)
                            }else{
                                callback(false, nil,nil,nil,nil,nil,nil)

                            }
                            
                  }
                        }
                    }
            }
    
    static func AddCar(_ imagesKey:String = "images[]", images : [UIImage] = [],car:CarData,param:[String: Any],callback:@escaping Callback ){
        var imgData:[Data]=[]


        var parameters=param
        
        parameters["payment_method"] = car.payment_method
        parameters["car_status"] = car.car_status
        parameters["mileage"] = car.mileage
        parameters["saloon_id"] = car.saloon_id
        parameters["fuel_id"] = car.fuel_id
        parameters["engine_id"] = car.engine_id
        parameters["car_type_id"] = car.car_type_id
        parameters["car_model"] = car.car_model
        parameters["year"] = car.year
        parameters["color"] = car.color
        parameters["price"] = car.price
        parameters["service_history_id"] = car.service_history_id
        parameters["country_id"] = car.country_id
        parameters["longitude"] = car.longitude
        parameters["latitude"] = car.latitude
        parameters["mobile"] = car.mobile
        parameters["name_en"] = car.name_en
        parameters["details_en"] = car.details_en


        AF.upload(multipartFormData: { (multipartFormData) in
            

            
            
            for img in images {
                let data =  img.jpegData(compressionQuality:0.50)
                
                multipartFormData.append(data!, withName: imagesKey, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
            
            
            
            
            for (key, value) in parameters {
                multipartFormData.append(("\(value)".data(using: String.Encoding.utf8))!, withName: key)
            }
        } ,to: API_KEYS.sellUcar.url, headers:  ["Accept-Language": "en","Authorization":"\(UserDefaults.standard.value(forKey: "Token") ?? "")"]).responseJSON { (response) in
            
               if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
             print(str)
                   print(data)
                        if let parsedMapperString : SellReq = Mapper<SellReq>().map(JSONString:str){
                            if parsedMapperString.status == true{
                                
                                callback(true,parsedMapperString.message ?? "")
                            }else{
                                callback(false, parsedMapperString.message ?? "")

                            }
                            
                  }
                        }
                    }
            }
    
    
    static func carDetails(id:Int , callback:@escaping (_ status:Bool,Car?,[Similar_cars]?) -> Void){

        let  carDetails = API_KEYS.carDetails.withId(id:id )
        AF.request(carDetails, method: .get ,headers: ["Accept-Language": "en"]).validate().responseJSON { [self] (response) in
               if let data  = response.data,let str : String = String(data: data, encoding: .utf8){
             print(str)
                   print(data)
                        if let parsedMapperString : CarDetailsData = Mapper<CarDetailsData>().map(JSONString:str){
                            if parsedMapperString.status == true{
                                
                                callback(true,parsedMapperString.car,parsedMapperString.similar_cars)
                            }else{
                                callback(false, nil,nil)

                            }
                            
                  }
                        }
                    }
            }
}
