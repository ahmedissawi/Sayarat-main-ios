//
//  MenuApiController.swift
//  Sayarat
//
//  Created by macbook on 8/24/21.
//

import Foundation
import ObjectMapper

class BaseGenericReponse<T: Mappable> : BaseResponse {
    
    var object : T?
    var list: [T]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        object <- map["object"]
        list <- map["list"]
    }
}
